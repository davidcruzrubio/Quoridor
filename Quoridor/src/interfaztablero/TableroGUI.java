/**
 * Fichero en cuyo interior está la representación de la clase TableroGUI,
 * la cual nos permite ver nuestro tablero, y los movimientos de los jugadores,
 * a través de nuestro monitor.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package interfaztablero;

import jade.core.AID;
import javax.swing.ImageIcon;

import juegosTablero.elementos.Posicion;
import tableroquoridor.AgenteTablero;

/**
 * Implementación de la propia clase.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio.
 */
public class TableroGUI extends javax.swing.JFrame {

    //Matriz de Casillas que representa al propio tablero//
    private CasillaGUI[][] matrix;

    //Variable almacena el número de jugadores que hay en el tablero//
    int nJugadores;
    //Variables que nos determinan si un jugador (por su color) está en el//
    //tablero o no//
    boolean jRojo, jAmarillo, jAzul, jVerde;

    //Variables de control para almacenar las casillas anteriores donde se encontraban//
    //los jugadores//
    private javax.swing.JLabel cAnteriorAmarillo;
    private javax.swing.JLabel cAnteriorRojo;
    private javax.swing.JLabel cAnteriorAzul;
    private javax.swing.JLabel cAnteriorVerde;

    int nBarrerasRojo; //Cantidad de paredes restantes del jugador Rojo
    int nBarrerasAmarillo; //Cantidad de paredes restantes del jugador Amarillo
    int nBarrerasAzul; //Cantidad de paredes restantes del jugador Azul
    int nBarrerasVerde; //Cantidad de paredes restantes del jugador Verde

    /**
     * Constructor de la clase TableroGUI
     *
     * @param _tablero
     */
    public TableroGUI(AgenteTablero _tablero) {
        super(_tablero.getLocalName());

        initComponents();

        nJugadores = 0;
        jRojo = jAmarillo = jAzul = jVerde = false;

        matrix = new CasillaGUI[9][9];
        //PRIMERA FILA//
        matrix[0][0] = new CasillaGUI(c00, p00, p00v);
        matrix[0][1] = new CasillaGUI(c01, p01, p01v);
        matrix[0][2] = new CasillaGUI(c02, p02, p02v);
        matrix[0][3] = new CasillaGUI(c03, p03, p03v);
        matrix[0][4] = new CasillaGUI(c04, p04, p04v);
        matrix[0][5] = new CasillaGUI(c05, p05, p05v);
        matrix[0][6] = new CasillaGUI(c06, p06, p06v);
        matrix[0][7] = new CasillaGUI(c07, p07, p07v);
        matrix[0][8] = new CasillaGUI(c08);
        //SEGUNDA FILA//
        matrix[1][0] = new CasillaGUI(c10, p10, p10v);
        matrix[1][1] = new CasillaGUI(c11, p11, p11v);
        matrix[1][2] = new CasillaGUI(c12, p12, p12v);
        matrix[1][3] = new CasillaGUI(c13, p13, p13v);
        matrix[1][4] = new CasillaGUI(c14, p14, p14v);
        matrix[1][5] = new CasillaGUI(c15, p15, p15v);
        matrix[1][6] = new CasillaGUI(c16, p16, p16v);
        matrix[1][7] = new CasillaGUI(c17, p17, p17v);
        matrix[1][8] = new CasillaGUI(c18);
        //TERCERA FILA//
        matrix[2][0] = new CasillaGUI(c20, p20, p20v);
        matrix[2][1] = new CasillaGUI(c21, p21, p21v);
        matrix[2][2] = new CasillaGUI(c22, p22, p22v);
        matrix[2][3] = new CasillaGUI(c23, p23, p23v);
        matrix[2][4] = new CasillaGUI(c24, p24, p24v);
        matrix[2][5] = new CasillaGUI(c25, p25, p25v);
        matrix[2][6] = new CasillaGUI(c26, p26, p26v);
        matrix[2][7] = new CasillaGUI(c27, p27, p27v);
        matrix[2][8] = new CasillaGUI(c28);
        //CUARTA FILA//
        matrix[3][0] = new CasillaGUI(c30, p30, p30v);
        matrix[3][1] = new CasillaGUI(c31, p31, p31v);
        matrix[3][2] = new CasillaGUI(c32, p32, p32v);
        matrix[3][3] = new CasillaGUI(c33, p33, p33v);
        matrix[3][4] = new CasillaGUI(c34, p34, p34v);
        matrix[3][5] = new CasillaGUI(c35, p35, p35v);
        matrix[3][6] = new CasillaGUI(c36, p36, p36v);
        matrix[3][7] = new CasillaGUI(c37, p37, p37v);
        matrix[3][8] = new CasillaGUI(c38);
        //QUINTA FILA//
        matrix[4][0] = new CasillaGUI(c40, p40, p40v);
        matrix[4][1] = new CasillaGUI(c41, p41, p41v);
        matrix[4][2] = new CasillaGUI(c42, p42, p42v);
        matrix[4][3] = new CasillaGUI(c43, p43, p43v);
        matrix[4][4] = new CasillaGUI(c44, p44, p44v);
        matrix[4][5] = new CasillaGUI(c45, p45, p45v);
        matrix[4][6] = new CasillaGUI(c46, p46, p46v);
        matrix[4][7] = new CasillaGUI(c47, p47, p47v);
        matrix[4][8] = new CasillaGUI(c48);
        //SEXTA FILA//
        matrix[5][0] = new CasillaGUI(c50, p50, p50v);
        matrix[5][1] = new CasillaGUI(c51, p51, p51v);
        matrix[5][2] = new CasillaGUI(c52, p52, p52v);
        matrix[5][3] = new CasillaGUI(c53, p53, p53v);
        matrix[5][4] = new CasillaGUI(c54, p54, p54v);
        matrix[5][5] = new CasillaGUI(c55, p55, p55v);
        matrix[5][6] = new CasillaGUI(c56, p56, p56v);
        matrix[5][7] = new CasillaGUI(c57, p57, p57v);
        matrix[5][8] = new CasillaGUI(c58);
        //SÉPTIMA FILA//
        matrix[6][0] = new CasillaGUI(c60, p60, p60v);
        matrix[6][1] = new CasillaGUI(c61, p61, p61v);
        matrix[6][2] = new CasillaGUI(c62, p62, p62v);
        matrix[6][3] = new CasillaGUI(c63, p63, p63v);
        matrix[6][4] = new CasillaGUI(c64, p64, p64v);
        matrix[6][5] = new CasillaGUI(c65, p65, p65v);
        matrix[6][6] = new CasillaGUI(c66, p66, p66v);
        matrix[6][7] = new CasillaGUI(c67, p67, p67v);
        matrix[6][8] = new CasillaGUI(c68);
        //OCTAVA FILA//
        matrix[7][0] = new CasillaGUI(c70, p70, p70v);
        matrix[7][1] = new CasillaGUI(c71, p71, p71v);
        matrix[7][2] = new CasillaGUI(c72, p72, p72v);
        matrix[7][3] = new CasillaGUI(c73, p73, p73v);
        matrix[7][4] = new CasillaGUI(c74, p74, p74v);
        matrix[7][5] = new CasillaGUI(c75, p75, p75v);
        matrix[7][6] = new CasillaGUI(c76, p76, p76v);
        matrix[7][7] = new CasillaGUI(c77, p77, p77v);
        matrix[7][8] = new CasillaGUI(c78);
        //NOVENA FILA//
        matrix[8][0] = new CasillaGUI(c80);
        matrix[8][1] = new CasillaGUI(c81);
        matrix[8][2] = new CasillaGUI(c82);
        matrix[8][3] = new CasillaGUI(c83);
        matrix[8][4] = new CasillaGUI(c84);
        matrix[8][5] = new CasillaGUI(c85);
        matrix[8][6] = new CasillaGUI(c86);
        matrix[8][7] = new CasillaGUI(c87);
        matrix[8][8] = new CasillaGUI(c88);

        setVisible(true);
        setLocationRelativeTo(null);

        cAnteriorAmarillo = c40;
        cAnteriorRojo = c04;
        cAnteriorAzul = c48;
        cAnteriorVerde = c84;

        TextoConsola.append("Bienvenidos Jugadores.\n");

        nBarrerasRojo = 0;
        nBarrerasAmarillo = 0;
        nBarrerasAzul = 0;
        nBarrerasVerde = 0;

    }

    /**
     * Función para colocar una barrera en una determinada posición y por un
     * jugador.
     *
     * @param p
     * @param orientacion
     * @param aid
     */
    public void establecerBarrera(Posicion p, String orientacion, AID aid) {
        if (orientacion.equals(juegoQuoridor.OntologiaQuoridor.ALINEACION_VERTICAL)) {
            matrix[p.getCoorX()][p.getCoorY()].activarBarrera(true);
            TextoConsola.append("El jugador " + aid.getLocalName() + " coloco una barrera vertical\n en la posicion "
                    + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
        } else if (orientacion.equals(juegoQuoridor.OntologiaQuoridor.ALINEACION_HORIZONTAL)) {
            matrix[p.getCoorX()][p.getCoorY()].activarBarrera(false);
            TextoConsola.append("El jugador " + aid.getLocalName() + " coloco una barrera horizontal\n en la posicion "
                    + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
        }
    }

    /**
     * Función para mover un jugador a una determinada posición.
     *
     * @param p
     * @param color
     * @param aid
     */
    public void moverJugador(Posicion p, String color, AID aid) {
        switch (color) {
            case juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_1:
                this.cAnteriorRojo.setIcon(new ImageIcon(getClass().getResource("/imagenes/casilla.jpg")));
                matrix[p.getCoorX()][p.getCoorY()].getC().setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorrojo.jpg")));
                this.cAnteriorRojo = matrix[p.getCoorX()][p.getCoorY()].getC();
                TextoConsola.append("El jugador " + aid.getLocalName() + " se movio a la posicion "
                        + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
                break;
            case juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_2:
                this.cAnteriorAzul.setIcon(new ImageIcon(getClass().getResource("/imagenes/casilla.jpg")));
                matrix[p.getCoorX()][p.getCoorY()].getC().setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorazul.jpg")));
                this.cAnteriorAzul = matrix[p.getCoorX()][p.getCoorY()].getC();
                TextoConsola.append("El jugador " + aid.getLocalName() + " se movio a la posicion "
                        + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
                break;
            case juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_3:
                this.cAnteriorVerde.setIcon(new ImageIcon(getClass().getResource("/imagenes/casilla.jpg")));
                matrix[p.getCoorX()][p.getCoorY()].getC().setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorverde.jpg")));
                this.cAnteriorVerde = matrix[p.getCoorX()][p.getCoorY()].getC();
                TextoConsola.append("El jugador " + aid.getLocalName() + " se movio a la posicion "
                        + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
                break;
            default:
                this.cAnteriorAmarillo.setIcon(new ImageIcon(getClass().getResource("/imagenes/casilla.jpg")));
                matrix[p.getCoorX()][p.getCoorY()].getC().setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadoramarillo.jpg")));
                this.cAnteriorAmarillo = matrix[p.getCoorX()][p.getCoorY()].getC();
                TextoConsola.append("El jugador " + aid.getLocalName() + " se movio a la posicion "
                        + "(" + p.getCoorX() + "," + p.getCoorY() + ")\n");
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MensajeGanador = new javax.swing.JLabel();
        c00 = new javax.swing.JLabel();
        c01 = new javax.swing.JLabel();
        c02 = new javax.swing.JLabel();
        c03 = new javax.swing.JLabel();
        c04 = new javax.swing.JLabel();
        c05 = new javax.swing.JLabel();
        c06 = new javax.swing.JLabel();
        c07 = new javax.swing.JLabel();
        c08 = new javax.swing.JLabel();
        c10 = new javax.swing.JLabel();
        c11 = new javax.swing.JLabel();
        c12 = new javax.swing.JLabel();
        c13 = new javax.swing.JLabel();
        c14 = new javax.swing.JLabel();
        c15 = new javax.swing.JLabel();
        c16 = new javax.swing.JLabel();
        c17 = new javax.swing.JLabel();
        c18 = new javax.swing.JLabel();
        c20 = new javax.swing.JLabel();
        c21 = new javax.swing.JLabel();
        c22 = new javax.swing.JLabel();
        c23 = new javax.swing.JLabel();
        c24 = new javax.swing.JLabel();
        c25 = new javax.swing.JLabel();
        c26 = new javax.swing.JLabel();
        c27 = new javax.swing.JLabel();
        c28 = new javax.swing.JLabel();
        c30 = new javax.swing.JLabel();
        c31 = new javax.swing.JLabel();
        c32 = new javax.swing.JLabel();
        c33 = new javax.swing.JLabel();
        c34 = new javax.swing.JLabel();
        c35 = new javax.swing.JLabel();
        c36 = new javax.swing.JLabel();
        c37 = new javax.swing.JLabel();
        c38 = new javax.swing.JLabel();
        c40 = new javax.swing.JLabel();
        c41 = new javax.swing.JLabel();
        c42 = new javax.swing.JLabel();
        c43 = new javax.swing.JLabel();
        c44 = new javax.swing.JLabel();
        c45 = new javax.swing.JLabel();
        c46 = new javax.swing.JLabel();
        c47 = new javax.swing.JLabel();
        c48 = new javax.swing.JLabel();
        c50 = new javax.swing.JLabel();
        c51 = new javax.swing.JLabel();
        c52 = new javax.swing.JLabel();
        c53 = new javax.swing.JLabel();
        c54 = new javax.swing.JLabel();
        c55 = new javax.swing.JLabel();
        c56 = new javax.swing.JLabel();
        c57 = new javax.swing.JLabel();
        c58 = new javax.swing.JLabel();
        c60 = new javax.swing.JLabel();
        c61 = new javax.swing.JLabel();
        c62 = new javax.swing.JLabel();
        c63 = new javax.swing.JLabel();
        c64 = new javax.swing.JLabel();
        c65 = new javax.swing.JLabel();
        c66 = new javax.swing.JLabel();
        c67 = new javax.swing.JLabel();
        c68 = new javax.swing.JLabel();
        c70 = new javax.swing.JLabel();
        c71 = new javax.swing.JLabel();
        c72 = new javax.swing.JLabel();
        c73 = new javax.swing.JLabel();
        c74 = new javax.swing.JLabel();
        c75 = new javax.swing.JLabel();
        c76 = new javax.swing.JLabel();
        c77 = new javax.swing.JLabel();
        c78 = new javax.swing.JLabel();
        c80 = new javax.swing.JLabel();
        c81 = new javax.swing.JLabel();
        c82 = new javax.swing.JLabel();
        c83 = new javax.swing.JLabel();
        c84 = new javax.swing.JLabel();
        c85 = new javax.swing.JLabel();
        c86 = new javax.swing.JLabel();
        c87 = new javax.swing.JLabel();
        c88 = new javax.swing.JLabel();
        p00 = new javax.swing.JLabel();
        p01 = new javax.swing.JLabel();
        p02 = new javax.swing.JLabel();
        p03 = new javax.swing.JLabel();
        p04 = new javax.swing.JLabel();
        p05 = new javax.swing.JLabel();
        p06 = new javax.swing.JLabel();
        p07 = new javax.swing.JLabel();
        p10 = new javax.swing.JLabel();
        p11 = new javax.swing.JLabel();
        p12 = new javax.swing.JLabel();
        p13 = new javax.swing.JLabel();
        p14 = new javax.swing.JLabel();
        p15 = new javax.swing.JLabel();
        p16 = new javax.swing.JLabel();
        p17 = new javax.swing.JLabel();
        p20 = new javax.swing.JLabel();
        p21 = new javax.swing.JLabel();
        p22 = new javax.swing.JLabel();
        p23 = new javax.swing.JLabel();
        p24 = new javax.swing.JLabel();
        p25 = new javax.swing.JLabel();
        p26 = new javax.swing.JLabel();
        p27 = new javax.swing.JLabel();
        p30 = new javax.swing.JLabel();
        p31 = new javax.swing.JLabel();
        p32 = new javax.swing.JLabel();
        p33 = new javax.swing.JLabel();
        p34 = new javax.swing.JLabel();
        p35 = new javax.swing.JLabel();
        p36 = new javax.swing.JLabel();
        p37 = new javax.swing.JLabel();
        p40 = new javax.swing.JLabel();
        p41 = new javax.swing.JLabel();
        p42 = new javax.swing.JLabel();
        p43 = new javax.swing.JLabel();
        p44 = new javax.swing.JLabel();
        p45 = new javax.swing.JLabel();
        p46 = new javax.swing.JLabel();
        p47 = new javax.swing.JLabel();
        p50 = new javax.swing.JLabel();
        p51 = new javax.swing.JLabel();
        p52 = new javax.swing.JLabel();
        p53 = new javax.swing.JLabel();
        p54 = new javax.swing.JLabel();
        p55 = new javax.swing.JLabel();
        p56 = new javax.swing.JLabel();
        p57 = new javax.swing.JLabel();
        p60 = new javax.swing.JLabel();
        p61 = new javax.swing.JLabel();
        p62 = new javax.swing.JLabel();
        p63 = new javax.swing.JLabel();
        p64 = new javax.swing.JLabel();
        p65 = new javax.swing.JLabel();
        p66 = new javax.swing.JLabel();
        p67 = new javax.swing.JLabel();
        p70 = new javax.swing.JLabel();
        p71 = new javax.swing.JLabel();
        p72 = new javax.swing.JLabel();
        p73 = new javax.swing.JLabel();
        p74 = new javax.swing.JLabel();
        p75 = new javax.swing.JLabel();
        p76 = new javax.swing.JLabel();
        p77 = new javax.swing.JLabel();
        p00v = new javax.swing.JLabel();
        p01v = new javax.swing.JLabel();
        p02v = new javax.swing.JLabel();
        p03v = new javax.swing.JLabel();
        p04v = new javax.swing.JLabel();
        p05v = new javax.swing.JLabel();
        p06v = new javax.swing.JLabel();
        p07v = new javax.swing.JLabel();
        p10v = new javax.swing.JLabel();
        p11v = new javax.swing.JLabel();
        p12v = new javax.swing.JLabel();
        p13v = new javax.swing.JLabel();
        p14v = new javax.swing.JLabel();
        p15v = new javax.swing.JLabel();
        p16v = new javax.swing.JLabel();
        p17v = new javax.swing.JLabel();
        p20v = new javax.swing.JLabel();
        p21v = new javax.swing.JLabel();
        p22v = new javax.swing.JLabel();
        p23v = new javax.swing.JLabel();
        p24v = new javax.swing.JLabel();
        p25v = new javax.swing.JLabel();
        p26v = new javax.swing.JLabel();
        p27v = new javax.swing.JLabel();
        p30v = new javax.swing.JLabel();
        p31v = new javax.swing.JLabel();
        p32v = new javax.swing.JLabel();
        p33v = new javax.swing.JLabel();
        p34v = new javax.swing.JLabel();
        p35v = new javax.swing.JLabel();
        p36v = new javax.swing.JLabel();
        p37v = new javax.swing.JLabel();
        p40v = new javax.swing.JLabel();
        p41v = new javax.swing.JLabel();
        p42v = new javax.swing.JLabel();
        p43v = new javax.swing.JLabel();
        p44v = new javax.swing.JLabel();
        p45v = new javax.swing.JLabel();
        p46v = new javax.swing.JLabel();
        p47v = new javax.swing.JLabel();
        p50v = new javax.swing.JLabel();
        p51v = new javax.swing.JLabel();
        p52v = new javax.swing.JLabel();
        p53v = new javax.swing.JLabel();
        p54v = new javax.swing.JLabel();
        p55v = new javax.swing.JLabel();
        p56v = new javax.swing.JLabel();
        p57v = new javax.swing.JLabel();
        p60v = new javax.swing.JLabel();
        p61v = new javax.swing.JLabel();
        p62v = new javax.swing.JLabel();
        p63v = new javax.swing.JLabel();
        p64v = new javax.swing.JLabel();
        p65v = new javax.swing.JLabel();
        p66v = new javax.swing.JLabel();
        p67v = new javax.swing.JLabel();
        p70v = new javax.swing.JLabel();
        p71v = new javax.swing.JLabel();
        p72v = new javax.swing.JLabel();
        p73v = new javax.swing.JLabel();
        p74v = new javax.swing.JLabel();
        p75v = new javax.swing.JLabel();
        p76v = new javax.swing.JLabel();
        p77v = new javax.swing.JLabel();
        PanelPrincipal = new javax.swing.JScrollPane();
        TextoConsola = new javax.swing.JTextArea();
        PanelRojo = new javax.swing.JScrollPane();
        TextoRojo = new javax.swing.JTextArea();
        PanelAzul = new javax.swing.JScrollPane();
        TextoAzul = new javax.swing.JTextArea();
        PanelVerde = new javax.swing.JScrollPane();
        TextoVerde = new javax.swing.JTextArea();
        PanelAmarillo = new javax.swing.JScrollPane();
        TextoAmarillo = new javax.swing.JTextArea();
        iconoSolfamidas = new javax.swing.JLabel();
        BordePanelPrincipal = new javax.swing.JLabel();
        BordeRojo = new javax.swing.JLabel();
        BordeAzul = new javax.swing.JLabel();
        BordeVerde = new javax.swing.JLabel();
        BordeAmarillo = new javax.swing.JLabel();
        FondoTablero = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAutoRequestFocus(false);
        setSize(new java.awt.Dimension(860, 700));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        MensajeGanador.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        MensajeGanador.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(MensajeGanador, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 430, 190));

        c00.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c00, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, 60));

        c01.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c01, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, -1, 60));

        c02.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c02, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, -1, 60));

        c03.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c03, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, -1, 60));

        c04.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c04, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, -1, 60));

        c05.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c05, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 10, -1, 60));

        c06.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c06, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, -1, 60));

        c07.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c07, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, -1, 60));

        c08.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c08, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 10, -1, 60));

        c10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, 60));

        c11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c11, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, -1, 60));

        c12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c12, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, -1, 60));

        c13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c13, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, -1, 60));

        c14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c14, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 70, -1, 60));

        c15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c15, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, -1, 60));

        c16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c16, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 70, -1, 60));

        c17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c17, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, -1, 60));

        c18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c18, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 70, -1, 60));

        c20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, -1, 60));

        c21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c21, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, -1, 60));

        c22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c22, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, -1, 60));

        c23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c23, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, -1, 60));

        c24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c24, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 130, -1, 60));

        c25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c25, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 130, -1, 60));

        c26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c26, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 130, -1, 60));

        c27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c27, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 130, -1, 60));

        c28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c28, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 130, -1, 60));

        c30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c30, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, -1, 60));

        c31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c31, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 190, -1, 60));

        c32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c32, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 190, -1, 60));

        c33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c33, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 190, -1, 60));

        c34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c34, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 190, -1, 60));

        c35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c35, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, -1, 60));

        c36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c36, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 190, -1, 60));

        c37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c37, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 190, -1, 60));

        c38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c38, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 190, -1, 60));

        c40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c40, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, -1, 60));

        c41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c41, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, -1, 60));

        c42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c42, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, -1, 60));

        c43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c43, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 250, -1, 60));

        c44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c44, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 250, -1, 60));

        c45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c45, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 250, -1, 60));

        c46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c46, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 250, -1, 60));

        c47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c47, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 250, -1, 60));

        c48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c48, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 250, -1, 60));

        c50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c50, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, -1, 60));

        c51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c51, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 310, -1, 60));

        c52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c52, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, -1, 60));

        c53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c53, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 310, -1, 60));

        c54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c54, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 310, -1, 60));

        c55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c55, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 310, -1, 60));

        c56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c56, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 310, -1, 60));

        c57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c57, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 310, -1, 60));

        c58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c58, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 310, -1, 60));

        c60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c60, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, -1, 60));

        c61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c61, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, -1, 60));

        c62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c62, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 370, -1, 60));

        c63.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c63, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 370, -1, 60));

        c64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c64, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 370, -1, 60));

        c65.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c65, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 370, -1, 60));

        c66.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c66, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 370, -1, 60));

        c67.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c67, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 370, -1, 60));

        c68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c68, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 370, -1, 60));

        c70.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c70, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 430, 50, 60));

        c71.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c71, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 430, -1, 60));

        c72.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c72, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 430, -1, 60));

        c73.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c73, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 430, -1, 60));

        c74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c74, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 430, -1, 60));

        c75.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c75, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 430, -1, 60));

        c76.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c76, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 430, -1, 60));

        c77.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c77, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 430, -1, 60));

        c78.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c78, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 430, -1, 60));

        c80.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c80, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, -1, 60));

        c81.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c81, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 490, -1, 60));

        c82.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c82, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 490, -1, 60));

        c83.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c83, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 490, -1, 60));

        c84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c84, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 490, -1, 60));

        c85.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c85, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 490, -1, 60));

        c86.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c86, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 490, -1, 60));

        c87.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c87, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 490, -1, 60));

        c88.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/casilla.jpg"))); // NOI18N
        getContentPane().add(c88, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 490, -1, 60));
        getContentPane().add(p00, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 64, 110, 10));
        getContentPane().add(p01, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 64, 110, 10));
        getContentPane().add(p02, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 64, 110, 10));
        getContentPane().add(p03, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 64, 110, 10));
        getContentPane().add(p04, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 64, 110, 10));
        getContentPane().add(p05, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 64, 110, 10));
        getContentPane().add(p06, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 64, 110, 10));
        getContentPane().add(p07, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 64, 110, 10));
        getContentPane().add(p10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 126, 110, 10));
        getContentPane().add(p11, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 126, 110, 10));
        getContentPane().add(p12, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 126, 110, 10));
        getContentPane().add(p13, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 126, 110, 10));
        getContentPane().add(p14, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 126, 110, 10));
        getContentPane().add(p15, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 126, 110, 10));
        getContentPane().add(p16, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 126, 110, 10));
        getContentPane().add(p17, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 126, 110, 10));
        getContentPane().add(p20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 186, 110, 10));
        getContentPane().add(p21, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 186, 110, 10));
        getContentPane().add(p22, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 186, 110, 10));
        getContentPane().add(p23, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 186, 110, 10));
        getContentPane().add(p24, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 186, 110, 10));
        getContentPane().add(p25, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 186, 110, 10));
        getContentPane().add(p26, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 186, 110, 10));
        getContentPane().add(p27, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 186, 110, 10));
        getContentPane().add(p30, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 246, 110, 10));
        getContentPane().add(p31, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 246, 110, 10));
        getContentPane().add(p32, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 246, 110, 10));
        getContentPane().add(p33, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 246, 110, 10));
        getContentPane().add(p34, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 246, 110, 10));
        getContentPane().add(p35, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 246, 110, 10));
        getContentPane().add(p36, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 246, 110, 10));
        getContentPane().add(p37, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 246, 110, 10));
        getContentPane().add(p40, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 306, 110, 10));
        getContentPane().add(p41, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 306, 110, 10));
        getContentPane().add(p42, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 306, 110, 10));
        getContentPane().add(p43, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 306, 110, 10));
        getContentPane().add(p44, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 306, 110, 10));
        getContentPane().add(p45, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 306, 110, 10));
        getContentPane().add(p46, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 306, 110, 10));
        getContentPane().add(p47, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 306, 110, 10));
        getContentPane().add(p50, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 366, 110, 10));
        getContentPane().add(p51, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 366, 110, 10));
        getContentPane().add(p52, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 366, 110, 10));
        getContentPane().add(p53, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 366, 110, 10));
        getContentPane().add(p54, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 366, 110, 10));
        getContentPane().add(p55, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 366, 110, 10));
        getContentPane().add(p56, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 366, 110, 10));
        getContentPane().add(p57, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 366, 110, 10));
        getContentPane().add(p60, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 426, 110, 10));
        getContentPane().add(p61, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 426, 110, 10));
        getContentPane().add(p62, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 426, 110, 10));
        getContentPane().add(p63, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 426, 110, 10));
        getContentPane().add(p64, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 426, 110, 10));
        getContentPane().add(p65, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 426, 110, 10));
        getContentPane().add(p66, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 426, 110, 10));
        getContentPane().add(p67, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 426, 110, 10));
        getContentPane().add(p70, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 486, 110, 10));
        getContentPane().add(p71, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 486, 110, 10));
        getContentPane().add(p72, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 486, 110, 10));
        getContentPane().add(p73, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 486, 110, 10));
        getContentPane().add(p74, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 486, 110, 10));
        getContentPane().add(p75, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 486, 110, 10));
        getContentPane().add(p76, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 486, 110, 10));
        getContentPane().add(p77, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 486, 110, 10));
        getContentPane().add(p00v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 14, 10, 110));
        getContentPane().add(p01v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 10, 110));
        getContentPane().add(p02v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 10, 120));
        getContentPane().add(p03v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 10, 120));
        getContentPane().add(p04v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 10, 120));
        getContentPane().add(p05v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 10, 120));
        getContentPane().add(p06v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 10, 120));
        getContentPane().add(p07v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, 10, 120));
        getContentPane().add(p10v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 70, 10, 120));
        getContentPane().add(p11v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 10, 120));
        getContentPane().add(p12v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 70, 10, 120));
        getContentPane().add(p13v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 70, 10, 120));
        getContentPane().add(p14v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 70, 10, 120));
        getContentPane().add(p15v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 10, 120));
        getContentPane().add(p16v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, 10, 120));
        getContentPane().add(p17v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 70, 10, 120));
        getContentPane().add(p20v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, 10, 120));
        getContentPane().add(p21v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 130, 10, 120));
        getContentPane().add(p22v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 130, 10, 120));
        getContentPane().add(p23v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 130, 10, 120));
        getContentPane().add(p24v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 130, 10, 120));
        getContentPane().add(p25v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 130, 10, 120));
        getContentPane().add(p26v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 130, 10, 120));
        getContentPane().add(p27v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, 10, 120));
        getContentPane().add(p30v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 190, 10, 120));
        getContentPane().add(p31v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 190, 10, 120));
        getContentPane().add(p32v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, 10, 120));
        getContentPane().add(p33v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 190, 10, 120));
        getContentPane().add(p34v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 190, 10, 120));
        getContentPane().add(p35v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 190, 10, 120));
        getContentPane().add(p36v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 190, 10, 120));
        getContentPane().add(p37v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 190, 10, 120));
        getContentPane().add(p40v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 250, 10, 120));
        getContentPane().add(p41v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 250, 10, 120));
        getContentPane().add(p42v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 250, 10, 120));
        getContentPane().add(p43v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 250, 10, 120));
        getContentPane().add(p44v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 250, 10, 120));
        getContentPane().add(p45v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 250, 10, 120));
        getContentPane().add(p46v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 250, 10, 120));
        getContentPane().add(p47v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 10, 120));
        getContentPane().add(p50v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 310, 10, 120));
        getContentPane().add(p51v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, 10, 120));
        getContentPane().add(p52v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 310, 10, 120));
        getContentPane().add(p53v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 310, 10, 120));
        getContentPane().add(p54v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 310, 10, 120));
        getContentPane().add(p55v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 310, 10, 120));
        getContentPane().add(p56v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 310, 10, 120));
        getContentPane().add(p57v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 310, 10, 120));
        getContentPane().add(p60v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 370, 10, 120));
        getContentPane().add(p61v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 370, 10, 120));
        getContentPane().add(p62v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 370, 10, 120));
        getContentPane().add(p63v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 370, 10, 120));
        getContentPane().add(p64v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 370, 10, 120));
        getContentPane().add(p65v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 370, 10, 120));
        getContentPane().add(p66v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 370, 10, 120));
        getContentPane().add(p67v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 370, 10, 120));
        getContentPane().add(p70v, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 430, 10, 120));
        getContentPane().add(p71v, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 430, 10, 120));
        getContentPane().add(p72v, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 430, 10, 120));
        getContentPane().add(p73v, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 430, 10, 120));
        getContentPane().add(p74v, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 430, 10, 120));
        getContentPane().add(p75v, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 430, 10, 120));
        getContentPane().add(p76v, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 430, 10, 120));
        getContentPane().add(p77v, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 430, 10, 120));

        TextoConsola.setEditable(false);
        TextoConsola.setColumns(20);
        TextoConsola.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        TextoConsola.setLineWrap(true);
        TextoConsola.setRows(5);
        PanelPrincipal.setViewportView(TextoConsola);

        getContentPane().add(PanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 30, 300, 500));

        PanelRojo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        PanelRojo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        TextoRojo.setEditable(false);
        TextoRojo.setColumns(20);
        TextoRojo.setLineWrap(true);
        TextoRojo.setRows(5);
        PanelRojo.setViewportView(TextoRojo);

        getContentPane().add(PanelRojo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 570, 170, 110));

        PanelAzul.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        PanelAzul.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        TextoAzul.setEditable(false);
        TextoAzul.setColumns(20);
        TextoAzul.setLineWrap(true);
        TextoAzul.setRows(5);
        PanelAzul.setViewportView(TextoAzul);

        getContentPane().add(PanelAzul, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 570, 170, 110));

        PanelVerde.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        PanelVerde.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        TextoVerde.setEditable(false);
        TextoVerde.setColumns(20);
        TextoVerde.setLineWrap(true);
        TextoVerde.setRows(5);
        PanelVerde.setViewportView(TextoVerde);

        getContentPane().add(PanelVerde, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 570, 170, 110));

        PanelAmarillo.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        PanelAmarillo.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        TextoAmarillo.setEditable(false);
        TextoAmarillo.setColumns(20);
        TextoAmarillo.setLineWrap(true);
        TextoAmarillo.setRows(5);
        PanelAmarillo.setViewportView(TextoAmarillo);

        getContentPane().add(PanelAmarillo, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 570, 170, 110));

        iconoSolfamidas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/solfamidas.png"))); // NOI18N
        getContentPane().add(iconoSolfamidas, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 560, 120, 120));

        BordePanelPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/fondo negro.jpg"))); // NOI18N
        getContentPane().add(BordePanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 20, 320, 520));

        BordeRojo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/rojo.png"))); // NOI18N
        getContentPane().add(BordeRojo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 560, 180, 120));

        BordeAzul.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/azul.jpg"))); // NOI18N
        getContentPane().add(BordeAzul, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 560, 180, 120));

        BordeVerde.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/verde.png"))); // NOI18N
        getContentPane().add(BordeVerde, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 560, 180, 120));

        BordeAmarillo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/amarillo.jpg"))); // NOI18N
        getContentPane().add(BordeAmarillo, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 560, 180, 120));

        FondoTablero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondos/fondo.jpg"))); // NOI18N
        getContentPane().add(FondoTablero, new org.netbeans.lib.awtextra.AbsoluteConstraints(-20, -20, 900, 720));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Función para añadir un jugador al tablero.
     *
     * @param color
     * @param nombre
     */
    public void insertarJugador(String color, String nombre) {
        switch (color) {
            case "Rojo":
                TextoRojo.append(nombre);
                TextoRojo.append("\n Paredes restantes: " + nBarrerasRojo + "\n");
                TextoConsola.append("Jugador " + nombre + " ha entrado en la partida.\n");
                nJugadores++;
                jRojo = true;
                c04.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorrojo.jpg")));
                break;
            case "Amarillo":
                TextoAmarillo.append(nombre);
                TextoAmarillo.append("\n Paredes restantes: " + nBarrerasAmarillo + "\n");
                TextoConsola.append("Jugador " + nombre + " ha entrado en la partida.\n");
                nJugadores++;
                jAmarillo = true;
                c40.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadoramarillo.jpg")));
                break;
            case "Verde":
                TextoVerde.append(nombre);
                TextoVerde.append("\n Paredes restantes: " + nBarrerasVerde + "\n");
                TextoConsola.append("Jugador " + nombre + " ha entrado en la partida.\n");
                nJugadores++;
                jVerde = true;
                c84.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorverde.jpg")));
                break;
            case "Azul":
                TextoAzul.append(nombre);
                TextoAzul.append("\n Paredes restantes: " + nBarrerasAzul + "\n");
                TextoConsola.append("Jugador " + nombre + " ha entrado en la partida.\n");
                nJugadores++;
                jAzul = true;
                c48.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadorazul.jpg")));
                break;

        }
    }

    /**
     * Función que se encarga de establecer el número de barreras que posee un
     * determinado jugador.
     *
     * @param color
     * @param n
     */
    public void setnBarreras(String color, int n) {
        switch (color) {
            case "Rojo":
                nBarrerasRojo = n;
                break;
            case "Negro":
                nBarrerasAmarillo = n;
                break;
            case "Verde":
                nBarrerasVerde = n;
                break;
            case "Azul":
                nBarrerasAzul = n;
                break;
        }
    }

    /**
     * Función que sirve para cambiar el icono de un jugador por otro que
     * simboliza el estado "descalificado".
     *
     * @param nombre
     * @param color
     */
    public void descalificar(String nombre, String color) {
        TextoConsola.append("Jugador " + nombre + " ha sido descalificado\n");
        switch (color) {
            case "Rojo":
                cAnteriorRojo.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadordescalificado.jpg")));
                break;
            case "Azul":
                cAnteriorAzul.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadordescalificado.jpg")));
                break;
            case "Verde":
                cAnteriorVerde.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadordescalificado.jpg")));
                break;
            default:
                cAnteriorAmarillo.setIcon(new ImageIcon(getClass().getResource("/imagenes/jugadores/jugadordescalificado.jpg")));
                break;
        }

    }

    /**
     * Función para añadir texto en la consola.
     *
     * @param color
     * @param nombre
     */
    public void setTextoConsola(String color, String nombre) {
        switch (color) {
            case "Rojo":
                TextoRojo.setText("");
                TextoRojo.append(nombre);
                TextoRojo.append("\n Paredes restantes: " + nBarrerasRojo + "\n");
                break;
            case "Negro":
                TextoAmarillo.setText("");
                TextoAmarillo.append(nombre);
                TextoAmarillo.append("\n Paredes restantes: " + nBarrerasAmarillo + "\n");
                break;
            case "Verde":
                TextoVerde.setText("");
                TextoVerde.append(nombre);
                TextoVerde.append("\n Paredes restantes: " + nBarrerasVerde + "\n");
                break;
            case "Azul":
                TextoAzul.setText("");
                TextoAzul.append(nombre);
                TextoAzul.append("\n Paredes restantes: " + nBarrerasAzul + "\n");
                break;
        }
    }

    /**
     * Función que se encarga de mostrar el ganador en nuestro tablero.
     *
     * @param aid
     */
    public void mostrarGanador(AID aid) {
        MensajeGanador.setText(aid.getLocalName() + " ha ganado la Partida");
    }

    /**
     * Función para cerrar la ventana.
     *
     * @param evt
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.dispose();
    }//GEN-LAST:event_formWindowClosing


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BordeAmarillo;
    private javax.swing.JLabel BordeAzul;
    private javax.swing.JLabel BordePanelPrincipal;
    private javax.swing.JLabel BordeRojo;
    private javax.swing.JLabel BordeVerde;
    private javax.swing.JLabel FondoTablero;
    private javax.swing.JLabel MensajeGanador;
    private javax.swing.JScrollPane PanelAmarillo;
    private javax.swing.JScrollPane PanelAzul;
    private javax.swing.JScrollPane PanelPrincipal;
    private javax.swing.JScrollPane PanelRojo;
    private javax.swing.JScrollPane PanelVerde;
    public javax.swing.JTextArea TextoAmarillo;
    public javax.swing.JTextArea TextoAzul;
    private javax.swing.JTextArea TextoConsola;
    public javax.swing.JTextArea TextoRojo;
    public javax.swing.JTextArea TextoVerde;
    private javax.swing.JLabel c00;
    private javax.swing.JLabel c01;
    private javax.swing.JLabel c02;
    private javax.swing.JLabel c03;
    private javax.swing.JLabel c04;
    private javax.swing.JLabel c05;
    private javax.swing.JLabel c06;
    private javax.swing.JLabel c07;
    private javax.swing.JLabel c08;
    private javax.swing.JLabel c10;
    private javax.swing.JLabel c11;
    private javax.swing.JLabel c12;
    private javax.swing.JLabel c13;
    private javax.swing.JLabel c14;
    private javax.swing.JLabel c15;
    private javax.swing.JLabel c16;
    private javax.swing.JLabel c17;
    private javax.swing.JLabel c18;
    private javax.swing.JLabel c20;
    private javax.swing.JLabel c21;
    private javax.swing.JLabel c22;
    private javax.swing.JLabel c23;
    private javax.swing.JLabel c24;
    private javax.swing.JLabel c25;
    private javax.swing.JLabel c26;
    private javax.swing.JLabel c27;
    private javax.swing.JLabel c28;
    private javax.swing.JLabel c30;
    private javax.swing.JLabel c31;
    private javax.swing.JLabel c32;
    private javax.swing.JLabel c33;
    private javax.swing.JLabel c34;
    private javax.swing.JLabel c35;
    private javax.swing.JLabel c36;
    private javax.swing.JLabel c37;
    private javax.swing.JLabel c38;
    private javax.swing.JLabel c40;
    private javax.swing.JLabel c41;
    private javax.swing.JLabel c42;
    private javax.swing.JLabel c43;
    private javax.swing.JLabel c44;
    private javax.swing.JLabel c45;
    private javax.swing.JLabel c46;
    private javax.swing.JLabel c47;
    private javax.swing.JLabel c48;
    private javax.swing.JLabel c50;
    private javax.swing.JLabel c51;
    private javax.swing.JLabel c52;
    private javax.swing.JLabel c53;
    private javax.swing.JLabel c54;
    private javax.swing.JLabel c55;
    private javax.swing.JLabel c56;
    private javax.swing.JLabel c57;
    private javax.swing.JLabel c58;
    private javax.swing.JLabel c60;
    private javax.swing.JLabel c61;
    private javax.swing.JLabel c62;
    private javax.swing.JLabel c63;
    private javax.swing.JLabel c64;
    private javax.swing.JLabel c65;
    private javax.swing.JLabel c66;
    private javax.swing.JLabel c67;
    private javax.swing.JLabel c68;
    private javax.swing.JLabel c70;
    private javax.swing.JLabel c71;
    private javax.swing.JLabel c72;
    private javax.swing.JLabel c73;
    private javax.swing.JLabel c74;
    private javax.swing.JLabel c75;
    private javax.swing.JLabel c76;
    private javax.swing.JLabel c77;
    private javax.swing.JLabel c78;
    private javax.swing.JLabel c80;
    private javax.swing.JLabel c81;
    private javax.swing.JLabel c82;
    private javax.swing.JLabel c83;
    private javax.swing.JLabel c84;
    private javax.swing.JLabel c85;
    private javax.swing.JLabel c86;
    private javax.swing.JLabel c87;
    private javax.swing.JLabel c88;
    private javax.swing.JLabel iconoSolfamidas;
    private javax.swing.JLabel p00;
    private javax.swing.JLabel p00v;
    private javax.swing.JLabel p01;
    private javax.swing.JLabel p01v;
    private javax.swing.JLabel p02;
    private javax.swing.JLabel p02v;
    private javax.swing.JLabel p03;
    private javax.swing.JLabel p03v;
    private javax.swing.JLabel p04;
    private javax.swing.JLabel p04v;
    private javax.swing.JLabel p05;
    private javax.swing.JLabel p05v;
    private javax.swing.JLabel p06;
    private javax.swing.JLabel p06v;
    private javax.swing.JLabel p07;
    private javax.swing.JLabel p07v;
    private javax.swing.JLabel p10;
    private javax.swing.JLabel p10v;
    private javax.swing.JLabel p11;
    private javax.swing.JLabel p11v;
    private javax.swing.JLabel p12;
    private javax.swing.JLabel p12v;
    private javax.swing.JLabel p13;
    private javax.swing.JLabel p13v;
    private javax.swing.JLabel p14;
    private javax.swing.JLabel p14v;
    private javax.swing.JLabel p15;
    private javax.swing.JLabel p15v;
    private javax.swing.JLabel p16;
    private javax.swing.JLabel p16v;
    private javax.swing.JLabel p17;
    private javax.swing.JLabel p17v;
    private javax.swing.JLabel p20;
    private javax.swing.JLabel p20v;
    private javax.swing.JLabel p21;
    private javax.swing.JLabel p21v;
    private javax.swing.JLabel p22;
    private javax.swing.JLabel p22v;
    private javax.swing.JLabel p23;
    private javax.swing.JLabel p23v;
    private javax.swing.JLabel p24;
    private javax.swing.JLabel p24v;
    private javax.swing.JLabel p25;
    private javax.swing.JLabel p25v;
    private javax.swing.JLabel p26;
    private javax.swing.JLabel p26v;
    private javax.swing.JLabel p27;
    private javax.swing.JLabel p27v;
    private javax.swing.JLabel p30;
    private javax.swing.JLabel p30v;
    private javax.swing.JLabel p31;
    private javax.swing.JLabel p31v;
    private javax.swing.JLabel p32;
    private javax.swing.JLabel p32v;
    private javax.swing.JLabel p33;
    private javax.swing.JLabel p33v;
    private javax.swing.JLabel p34;
    private javax.swing.JLabel p34v;
    private javax.swing.JLabel p35;
    private javax.swing.JLabel p35v;
    private javax.swing.JLabel p36;
    private javax.swing.JLabel p36v;
    private javax.swing.JLabel p37;
    private javax.swing.JLabel p37v;
    private javax.swing.JLabel p40;
    private javax.swing.JLabel p40v;
    private javax.swing.JLabel p41;
    private javax.swing.JLabel p41v;
    private javax.swing.JLabel p42;
    private javax.swing.JLabel p42v;
    private javax.swing.JLabel p43;
    private javax.swing.JLabel p43v;
    private javax.swing.JLabel p44;
    private javax.swing.JLabel p44v;
    private javax.swing.JLabel p45;
    private javax.swing.JLabel p45v;
    private javax.swing.JLabel p46;
    private javax.swing.JLabel p46v;
    private javax.swing.JLabel p47;
    private javax.swing.JLabel p47v;
    private javax.swing.JLabel p50;
    private javax.swing.JLabel p50v;
    private javax.swing.JLabel p51;
    private javax.swing.JLabel p51v;
    private javax.swing.JLabel p52;
    private javax.swing.JLabel p52v;
    private javax.swing.JLabel p53;
    private javax.swing.JLabel p53v;
    private javax.swing.JLabel p54;
    private javax.swing.JLabel p54v;
    private javax.swing.JLabel p55;
    private javax.swing.JLabel p55v;
    private javax.swing.JLabel p56;
    private javax.swing.JLabel p56v;
    private javax.swing.JLabel p57;
    private javax.swing.JLabel p57v;
    private javax.swing.JLabel p60;
    private javax.swing.JLabel p60v;
    private javax.swing.JLabel p61;
    private javax.swing.JLabel p61v;
    private javax.swing.JLabel p62;
    private javax.swing.JLabel p62v;
    private javax.swing.JLabel p63;
    private javax.swing.JLabel p63v;
    private javax.swing.JLabel p64;
    private javax.swing.JLabel p64v;
    private javax.swing.JLabel p65;
    private javax.swing.JLabel p65v;
    private javax.swing.JLabel p66;
    private javax.swing.JLabel p66v;
    private javax.swing.JLabel p67;
    private javax.swing.JLabel p67v;
    private javax.swing.JLabel p70;
    private javax.swing.JLabel p70v;
    private javax.swing.JLabel p71;
    private javax.swing.JLabel p71v;
    private javax.swing.JLabel p72;
    private javax.swing.JLabel p72v;
    private javax.swing.JLabel p73;
    private javax.swing.JLabel p73v;
    private javax.swing.JLabel p74;
    private javax.swing.JLabel p74v;
    private javax.swing.JLabel p75;
    private javax.swing.JLabel p75v;
    private javax.swing.JLabel p76;
    private javax.swing.JLabel p76v;
    private javax.swing.JLabel p77;
    private javax.swing.JLabel p77v;
    // End of variables declaration//GEN-END:variables
}
