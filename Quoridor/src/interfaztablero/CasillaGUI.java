/**
 * Este fichero representa la clase CasillaGUI, la cual se usa dentro de TableroGUI
 * siendo estas las casillas de nuestra matriz que representa el tablero gráfico.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package interfaztablero;

import javax.swing.JLabel;

/**
 * Clase "CasillaGUI" necesaria para acoplar la interfaz a la estructura
 * necesaria para el juego.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
public class CasillaGUI {

    private JLabel c;
    private Pared vertical;
    private Pared horizontal;

    /**
     * Primer constructor parametrizado para la clase.
     *
     * @param _c
     */
    public CasillaGUI(JLabel _c) {
        c = _c;
    }

    /**
     * Segundo constructor parametrizado de la clase.
     *
     * @param _c
     * @param _pH
     * @param _pV
     */
    public CasillaGUI(JLabel _c, JLabel _pH, JLabel _pV) {
        c = _c;
        vertical = new Pared(_pV);
        horizontal = new Pared(_pH);
    }
    
    /**
     * Función que permite activar una barrera dentro de la casilla.
     * @param pared 
     */
    public void activarBarrera(boolean pared) {
        if (pared && vertical != null && !horizontal.getActivada()) {
            vertical.activarBarrera();
        } else if (horizontal != null && !vertical.getActivada()) {
            horizontal.activarBarrera();
        }
    }

    /**
     * Obetenemos la casilla "JLabel".
     *
     * @return the c
     */
    public JLabel getC() {
        return c;
    }

    /**
     * Establecemos la casilla "JLabel".
     *
     * @param c the c to set
     */
    public void setC(JLabel c) {
        this.c = c;
    }

}
