/**
 * Fichero que posee dentro la clase Pared, necesaria a la hora de representar
 * paredes dentro de la clase CasillaGUI
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package interfaztablero;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Clase que encapsula el concepto de "Pared" en el tablero.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
public class Pared {

    //Label asociado a cada pared//
    private JLabel p;
    //Variable que almacena su estado//
    private boolean activada;

    /**
     * Constructor de la clase Pared
     *
     * @param _pared
     */
    public Pared(JLabel _pared) {
        p = _pared;
    }

    /**
     * Función que nos informa de si una barrera está activada o no.
     *
     * @return
     */
    public boolean getActivada() {
        return activada;
    }

    /**
     * Función que activa la barrera y cambia su icono.
     *
     */
    public void activarBarrera() {
        activada = true;
        p.setIcon(new ImageIcon(getClass().getResource("/imagenes/pared.jpg")));
    }
}
