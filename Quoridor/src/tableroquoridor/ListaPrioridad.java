/**
 * Fichero con el que se pretende implementar la funcionalidad que ofrece
 * una estructura de datos con forma de Lista de prioridad, necesaria para escoger
 * a los jugadores a la hora de comenzar una partida.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package tableroquoridor;

import jade.core.AID;
import java.util.ArrayList;

/**
 * Clase que implementa la estructura de datos: Lista con prioridad.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
public class ListaPrioridad {

    //Atributo principal, es una lista de listas de AIDs//
    private final ArrayList<ArrayList<AID>> listaPrioridad;

    /**
     * Constructor de la clase.
     */
    public ListaPrioridad() {
        listaPrioridad = new ArrayList<ArrayList<AID>>();
    }

    /**
     * Función que permite insertar un jugador gracias a su AID y su prioridad.
     *
     * @param aid
     * @param prioridad
     */
    public void insertarJugador(AID aid, int prioridad) {
        //Agrandamos la lista si esa prioridad no existe
        if (prioridad >= listaPrioridad.size()) {
            for (int i = listaPrioridad.size(); i <= prioridad; i++) {
                listaPrioridad.add(i, new ArrayList<AID>());
            }
        }
        //Insertamos jugador
        listaPrioridad.get(prioridad).add(aid);
    }

    /**
     * Función que obtiene el jugador con mayor prioridad.
     *
     * @return
     */
    public AID getJugadorPrioritario() {
        for (int i = 0; i < listaPrioridad.size(); i++) {
            if (!listaPrioridad.get(i).isEmpty()) {
                return listaPrioridad.get(i).remove(0);
            }
        }
        return null;
    }

    /**
     * Función que determina si la lista está vacía o no.
     *
     * @return
     */
    public Boolean vacia() {
        for (int i = 0; i < listaPrioridad.size(); i++) {
            if (!listaPrioridad.get(i).isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
