/**
 * Fichero que posee todas la características de nuestro Tablero.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package tableroquoridor;

import interfaztablero.FormularioInicial;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetInitiator;
import jade.proto.ProposeInitiator;
import jade.proto.SubscriptionResponder;
import jade.proto.SubscriptionResponder.Subscription;
import jade.proto.SubscriptionResponder.SubscriptionManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import juegoQuoridor.OntologiaQuoridor;
import juegoQuoridor.elementos.FichaEntregada;
import juegosTablero.elementos.Ficha;
import juegosTablero.elementos.Partida;
import juegosTablero.elementos.ProponerPartida;
import juegosTablero.elementos.Tablero;
import juegosTablero.elementos.Posicion;
import juegosTablero.elementos.Jugador;
import juegoQuoridor.elementos.Movimiento;
import juegoQuoridor.elementos.JugarPartida;
import juegoQuoridor.elementos.MovimientoRealizado;
import juegoQuoridor.elementos.Muro;
import juegosTablero.elementos.GanadorPartida;

/**
 * Clase que representa al agente Tablero implementado en nuestro proyecto
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
public class AgenteTablero extends Agent {

    // Atributos referentes a la UI//
    private FormularioInicial initForm;

    // Atributos referentes a la Ontología//
    private Codec codec = new SLCodec();
    private Ontology ontologia;
    private ContentManager manager = (ContentManager) getContentManager();

    // Atributos propios del agente//
    private ArrayList<AID> jugadores;
    private int jugadoresPref = -1;

    private ArrayList<PartidaQuoridor> partidas;
    private int idPartida = 0;
    private Map<AID, Boolean> estaJugando;
    private Map<AID, Integer> vecesJugadas;

    //Estructura de datos necesaria para refrescar la información gráfica del
    //tablero.
    private HashMap<String, ArrayList<MovimientoRealizado>> colaMovimientos;

    //Estructura de datos necesaria para gestionar el "Susbcription Manager".
    private HashSet<Subscription> suscripciones;

    //Estructura de datos necesaria para la suscripción//
    private HashMap<String, ArrayList<AID>> tableroJugador;

    @Override
    protected void setup() {
        //Reservamos el espacio en memoria para las estructuras usadas//
        suscripciones = new HashSet<>();
        partidas = new ArrayList<>();
        tableroJugador = new HashMap<>();
        estaJugando = new HashMap<>();
        vecesJugadas = new HashMap<>();
        jugadores = new ArrayList<>();
        colaMovimientos = new HashMap<>();

        try {
            ontologia = juegoQuoridor.OntologiaQuoridor.getInstance();
        } catch (BeanOntologyException ex) {
            Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Configuración del GUI//
        initForm = new FormularioInicial(this);

        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(juegoQuoridor.OntologiaQuoridor.REGISTRO_TABLERO);
        sd.setName("Tablero");
        sd.addOntologies(juegoQuoridor.OntologiaQuoridor.ONTOLOGY_NAME);
        sd.addLanguages(FIPANames.ContentLanguage.FIPA_SL);
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        manager.registerLanguage(codec);
        manager.registerOntology(ontologia);

        //Añadimos las tareas básicas para buscar jugadores, parte gráfica y suscripción//
        addBehaviour(new buscarJugadores(this, 10000));
        addBehaviour(new refrescarTableros(this, 1000));
        addBehaviour(new lanzarSuscripcion());
    }

    @Override
    protected void takeDown() {
        //Desregristo del agente de las Páginas Amarillas//
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            ex.printStackTrace();
        }
        //Despedida//
        System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }

    // ---- MÉTODOS ---- //
    /**
     * Método que se llama una vez que se ha elegido el número de jugadores
     * preferente desde el formulario inicial.
     *
     */
    public void iniciarProponerPartida() {
        addBehaviour(new proponerPartida(this, jugadoresPref, jugadores));
        for (int i = 0; i < jugadores.size(); i++) {
            System.out.println(jugadores.get(i).getLocalName());
        }
    }

    /**
     * Método para establecer el número de jugadores preferentes y comunicarse
     * con el formulario inicial.
     *
     * @param jp
     */
    public void setJugadoresPref(String jp) {
        jugadoresPref = Integer.parseInt(jp);
        if (jugadores != null && jugadores.size() >= jugadoresPref) {
            initForm.activarBotonAceptar(jugadores.size());
        } else {
            initForm.desactivarBotonAceptar();
        }
    }

    // ---- CLASES PARA LA COMUNICACIÓN ---- //
    /**
     * Clase que representa la tarea encargada de buscar los jugadores para
     * poder iniciar una partida.
     *
     */
    protected class buscarJugadores extends TickerBehaviour {

        public buscarJugadores(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setType(juegoQuoridor.OntologiaQuoridor.REGISTRO_JUGADOR);
            template.addServices(sd);
            try {
                DFAgentDescription[] result = DFService.search(myAgent, template);
                jugadores.clear();
                for (int i = 0; i < result.length; ++i) {
                    if (!estaJugando.getOrDefault(result[i].getName(), false)) {
                        jugadores.add(result[i].getName());
                    }
                }
                System.out.println("Cantidad de jugadores esperando a jugar: " + jugadores.size());

                if (jugadoresPref != -1 && jugadores.size() >= jugadoresPref) {
                    initForm.activarBotonAceptar(jugadores.size());
                } else {
                    initForm.desactivarBotonAceptar();
                }

            } catch (FIPAException fe) {
                fe.printStackTrace();
            }
        }
    }

    /**
     * Clase proponer partida que representa una tarea de un disparo necesaria
     * para poder iniciar el protocolo Contract Net.
     *
     */
    protected class proponerPartida extends OneShotBehaviour {

        private int numJugadores;
        private ArrayList<AID> jugadoresDisponibles;

        public proponerPartida(Agent a, int nj, ArrayList<AID> je) {
            super(a);
            numJugadores = nj;
            jugadoresDisponibles = je;
        }

        @Override
        public void action() {
            //Creamos el mensaje cfp inicial que se envia en el CONTRACTNET
            ACLMessage msg = new ACLMessage(ACLMessage.CFP);
            for (int i = 0; i < jugadoresDisponibles.size(); i++) {
                msg.addReceiver(jugadoresDisponibles.get(i));  //Enviamos a todos, dentro del contract net//
            }                                   //gestionara la prioridad de los que contestan//

            msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
            msg.setLanguage(codec.getName());
            msg.setOntology(ontologia.getName());

            //Preparar el Contenido del mensaje//
            Tablero tablero = new Tablero(9, 9);
            Partida partida = new Partida(
                    Integer.toString(idPartida),
                    juegoQuoridor.OntologiaQuoridor.TIPO_JUEGO,
                    numJugadores,
                    tablero);

            ProponerPartida p = new ProponerPartida(partida);

            //Aqui indicamos el timeout de 1 seg en el que los jugadores deben contestar//
            msg.setReplyByDate(new Date(System.currentTimeMillis() + 1000));

            try {
                //Enviamos el mensaje (llamamos al iniciador del ContractNet)//
                Action a = new Action(myAgent.getAID(), p);
                manager.fillContent(msg, a);
                addBehaviour(new IniciadorContractNet(myAgent, msg, numJugadores, Integer.toString(idPartida), 0));
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
            }
            idPartida++;
        }
    }

    /**
     * Clase que encapsula la funcionalidad necesaria para realizar el protocolo
     * Contract Net, el cual es el encargado de proponer la partida a los
     * jugadores.
     *
     */
    private class IniciadorContractNet extends ContractNetInitiator {

        private ACLMessage cfp;
        private ArrayList<Pair<AID, Ficha>> jugadoresPartida;
        private int numJugadores;
        private String IDpartida;
        private int intentos;

        public IniciadorContractNet(Agent a, ACLMessage _cfp, int nj, String _IDpartida, int _intentos) {
            super(a, _cfp);
            cfp = _cfp;
            numJugadores = nj;
            IDpartida = _IDpartida;
            intentos = _intentos;
        }

        //Método colectivo llamado tras finalizar el tiempo de espera//
        //o recibir todas las propuestas.//
        @Override
        protected void handleAllResponses(Vector respuestas, Vector aceptados) {
            //CASO 1: No han respondio suficientes jugadores//
            if (respuestas.size() < numJugadores || respuestas.isEmpty()) {
                System.out.printf("No se alcanzo el numero de jugadores, REINICIAMOS 1\n");
                if (intentos < 6) {
                    addBehaviour(new IniciadorContractNet(myAgent, cfp, numJugadores, IDpartida, intentos + 1));
                }
            } else {
                //Comprobamos cuantos han respondido afirmativamente(PROPOSE)//
                ListaPrioridad list = new ListaPrioridad();
                ArrayList<AID> proposeRecibidos = new ArrayList<AID>();
                ArrayList<ACLMessage> resp = new ArrayList<ACLMessage>();
                for (int i = 0; i < respuestas.size(); i++) {
                    ACLMessage msg = (ACLMessage) respuestas.get(i);
                    if (msg.getPerformative() == ACLMessage.PROPOSE) {
                        proposeRecibidos.add(msg.getSender());
                        resp.add(msg.createReply());
                    }
                }
                //CASO 1.2: No han respondio suficientes jugadores con propose//
                if (proposeRecibidos.size() < numJugadores) {
                    System.out.printf("No se alcanzo el numero de jugadores, REINICIAMOS 2\n");
                    if (intentos < 6) {
                        addBehaviour(new IniciadorContractNet(myAgent, cfp, numJugadores, IDpartida, intentos + 1));
                    }
                } // CASO 2: Han respondido suficientes jugadores//
                else {
                    for (int i = 0; i < proposeRecibidos.size(); i++) {
                        list.insertarJugador(proposeRecibidos.get(i),
                                vecesJugadas.getOrDefault(proposeRecibidos.get(i), 0));
                    }

                    jugadoresPartida = new ArrayList<Pair<AID, Ficha>>();
                    for (int i = 0; i < numJugadores; i++) {
                        AID j = list.getJugadorPrioritario();

                        int k = 0;
                        for (int m = 0; m < resp.size(); m++) {
                            if (proposeRecibidos.get(m) == j) {
                                k = m;
                            }
                        }
                        resp.get(k).setPerformative(ACLMessage.ACCEPT_PROPOSAL);

                        Ficha ficha = null;
                        if (i == 0) {
                            ficha = new Ficha(juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_1);
                        }
                        if (i == 2) {
                            ficha = new Ficha(juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_2);
                        }
                        if (i == 1) {
                            ficha = new Ficha(juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_3);
                        }
                        if (i == 3) {
                            ficha = new Ficha(juegoQuoridor.OntologiaQuoridor.COLOR_FICHA_4);
                        }

                        jugadoresPartida.add(new Pair<AID, Ficha>(j, ficha));

                        try {
                            FichaEntregada a = new FichaEntregada(ficha, i + 1);
                            manager.fillContent(resp.get(k), a);
                            aceptados.add(resp.get(k));
                        } catch (Codec.CodecException | OntologyException ex) {
                            Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    int i = 0;
                    while (!list.vacia()) {
                        AID j = list.getJugadorPrioritario();
                        int k = 0;
                        for (int m = 0; m < resp.size(); m++) {
                            if (proposeRecibidos.get(m) == j) {
                                k = m;
                            }
                        }
                        resp.get(k).setPerformative(ACLMessage.REJECT_PROPOSAL);
                        aceptados.add(resp.get(k));
                        i++;
                    }
                }
            }
        }

        @Override
        protected void handleFailure(ACLMessage fallo) {
            //Un jugador ha fallado, hay que reiniciar//
            System.out.printf("No se alcanzo el numero de jugadores, REINICIAMOS 3\n");
            addBehaviour(new IniciadorContractNet(myAgent, cfp, jugadoresPartida.size(), IDpartida, 0));
        }

        @Override
        protected void handleInform(ACLMessage inform) {
            numJugadores--;
            if (numJugadores == 0) {
                PartidaQuoridor partida = new PartidaQuoridor((AgenteTablero) myAgent, IDpartida);
                for (int i = 0; i < jugadoresPartida.size(); i++) {
                    partida.insertarJugador(jugadoresPartida.get(i).getKey());
                    estaJugando.put(jugadoresPartida.get(i).getKey(), true);
                    vecesJugadas.put(jugadoresPartida.get(i).getKey(), (vecesJugadas.getOrDefault(jugadoresPartida.get(i).getKey(),
                            0)) + 1);
                    partida.comprobarCantidadBarreras(jugadoresPartida.get(i).getKey(), jugadoresPartida.get(i).getValue().getColor(), jugadoresPartida.size());
                    partida.asignarColor(jugadoresPartida.get(i).getKey(),
                            jugadoresPartida.get(i).getValue().getColor());

                }
                partidas.add(partida);
                addBehaviour(new jugarPartida(myAgent, partida));
            }
        }
    }

    /**
     * Tarea "OneShot" encargada de iniciar el protocolo Propose encargado de
     * jugar la partida, creando el mensaje necesario.
     *
     */
    protected class jugarPartida extends OneShotBehaviour {

        private PartidaQuoridor partida;

        public jugarPartida(Agent a, PartidaQuoridor _partida) {
            super(a);
            partida = _partida;
        }

        @Override
        public void action() {
            ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE);
            ArrayList<AID> js = partida.getJugadores();

            for (int i = 0; i < js.size(); i++) {
                msg.addReceiver(js.get(i));  //Enviamos a todos jugadores la lista de AIDs 
            }

            msg.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
            msg.setLanguage(codec.getName());
            msg.setOntology(ontologia.getName());

            //Preparar el Contenido del mensaje  
            Tablero tablero = new Tablero(9, 9);
            Partida msgPartida = new Partida(
                    partida.getIdPartida(),
                    juegoQuoridor.OntologiaQuoridor.TIPO_JUEGO,
                    partida.getNjugadores(),
                    tablero);

            Movimiento movimiento = new Movimiento(
                    null, // private Object elementoJuego;
                    new Posicion(-1, -1)
            );

            AID jAid = partida.getJugador(0);
            Jugador jugador = new Jugador(
                    jAid,
                    new Ficha(partida.getColor(jAid))
            );

            //Aqui indicamos el timeout de 1 seg en el que los jugadores deben contestar 
            msg.setReplyByDate(new Date(System.currentTimeMillis() + 1000));
            JugarPartida p = new JugarPartida(msgPartida, movimiento, jugador);

            try {
                //Enviamos el mensaje (llamamos al iniciador del Propose)
                Action a = new Action(myAgent.getAID(), p);
                manager.fillContent(msg, a);
                addBehaviour(new IniciadorPropose(myAgent, msg, partida, 0, 0));
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Clase que se encarga de implementar el protocolo Propose, necesario para
     * que los jugadores se turnen y jueguen la partida.
     *
     */
    private class IniciadorPropose extends ProposeInitiator {

        private int turnoJugador;
        PartidaQuoridor partida;
        private int rotacion;

        public IniciadorPropose(Agent a, ACLMessage _mensaje,
                PartidaQuoridor _partida, int _turnoJugador, int _rotacion) {
            super(a, _mensaje);
            partida = _partida;
            turnoJugador = _turnoJugador;
            rotacion = _rotacion;
        }

        @Override
        protected void handleAllResponses(Vector respuestas) {
            if (respuestas.isEmpty()) {
                addBehaviour(new NotificarGanador(null, partida));
            }
            boolean haGanado = false;
            Jugador ganador = null;
            ArrayList<AID> aux = partida.getJugadores();
            for (int i = 0; i < respuestas.size(); i++) {
                for (int j = 0; j < aux.size(); j++) {
                    ACLMessage resp = (ACLMessage) respuestas.get(i);
                    if (resp.getSender().getName().equals(aux.get(j).getName())) {
                        aux.remove(j);
                    }
                }

                ACLMessage msg = (ACLMessage) respuestas.get(i);
                if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                    try {
                        ContentElement c = manager.extractContent(msg);
                        if (c instanceof MovimientoRealizado) {
                            MovimientoRealizado mr;
                            mr = (MovimientoRealizado) c;
                            ArrayList<MovimientoRealizado> lista;
                            if (colaMovimientos.containsKey(partida.getIdPartida())) {
                                lista = colaMovimientos.get(partida.getIdPartida());
                                lista.add(mr);
                            } else {
                                lista = new ArrayList<>();
                                lista.add(mr);

                            }
                            colaMovimientos.put(partida.getIdPartida(), lista);

                            //Información de jugador//
                            Jugador player = mr.getJugador();
                            String color = player.getFicha().getColor();

                            //Información del movimiento//
                            Movimiento movimiento = mr.getMovimiento();
                            Posicion posicion = movimiento.getPosicion();
                            Object objetoJuego = movimiento.getElementoJuego();

                            if (objetoJuego instanceof Jugador) {
                                switch (color) {
                                    case OntologiaQuoridor.COLOR_FICHA_1:
                                        if (posicion.getCoorX() == 8) {
                                            haGanado = true;
                                            ganador = player;
                                        }
                                        break;
                                    case OntologiaQuoridor.COLOR_FICHA_2:
                                        if (posicion.getCoorY() == 0) {
                                            haGanado = true;
                                            ganador = player;
                                        }
                                        break;
                                    case OntologiaQuoridor.COLOR_FICHA_3:
                                        if (posicion.getCoorX() == 0) {
                                            haGanado = true;
                                            ganador = player;
                                        }
                                        break;
                                    case OntologiaQuoridor.COLOR_FICHA_4:
                                        if (posicion.getCoorY() == 8) {
                                            haGanado = true;
                                            ganador = player;
                                        }
                                        break;
                                }
                            }
                        }
                    } catch (Codec.CodecException | OntologyException ex) {
                        Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (haGanado) {
                addBehaviour(new NotificarGanador(ganador, partida));
            } else {
                if (!aux.isEmpty()) {
                    for (int k = 0; k < aux.size(); k++) {
                        partida.descalificar(aux.get(k));
                    }
                }

                //Construimos el nuevo mensaje(nuevoTurno)//
                ACLMessage nuevoTurno = new ACLMessage(ACLMessage.PROPOSE);

                //Agregamos en receptores los Jugadores no descalificados//
                ArrayList<AID> receptores = partida.getJugadores();
                for (int i = 0; i < receptores.size(); i++) {
                    nuevoTurno.addReceiver(receptores.get(i));
                }

                nuevoTurno.setProtocol(FIPANames.InteractionProtocol.FIPA_PROPOSE);
                nuevoTurno.setLanguage(codec.getName());
                nuevoTurno.setOntology(ontologia.getName());

                //Preparar el Contenido del mensaje//
                Tablero tablero = new Tablero(9, 9);
                Partida msgPartida = new Partida(
                        partida.getIdPartida(),
                        juegoQuoridor.OntologiaQuoridor.TIPO_JUEGO,
                        partida.getNjugadores(),
                        tablero);

                int tam = colaMovimientos.get(partida.getIdPartida()).size();
                Movimiento movimiento = colaMovimientos.get(partida.getIdPartida()).get(tam - 1).getMovimiento();

                turnoJugador++;
                turnoJugador = turnoJugador % partida.getNjugadores();
                AID jAid = partida.getJugador(turnoJugador);
                Jugador jugador = new Jugador(
                        jAid,
                        new Ficha(partida.getColor(jAid))
                );

                //Aqui indicamos el timeout de 1 seg en el que los jugadores deben contestar//
                nuevoTurno.setReplyByDate(new Date(System.currentTimeMillis() + 1000));
                JugarPartida p = new JugarPartida(msgPartida, movimiento, jugador);

                try {
                    //Enviamos el mensaje (llamamos al iniciador del Propose de nuevo)//
                    Action a = new Action(myAgent.getAID(), p);
                    manager.fillContent(nuevoTurno, a);
                    addBehaviour(new IniciadorPropose(myAgent, nuevoTurno, partida, turnoJugador, rotacion));
                } catch (Codec.CodecException | OntologyException ex) {
                    Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Clases necesarias para lanzar la Suscripción.
     *
     */
    protected class lanzarSuscripcion extends OneShotBehaviour {

        public lanzarSuscripcion() {
        }

        @Override
        public void action() {
            MessageTemplate template = SubscriptionResponder.createMessageTemplate(ACLMessage.SUBSCRIBE);
            SubscriptionManager gestor = new SubscriptionManager() {

                @Override
                public boolean register(Subscription suscripcion) {
                    suscripciones.add(suscripcion);
                    return true;
                }

                @Override
                public boolean deregister(Subscription suscripcion) {
                    suscripciones.remove(suscripcion);
                    return true;
                }
            };
            addBehaviour(new GanarPartida(myAgent, template, gestor));
        }
    }

    /**
     * Clase con la funcionalidad necesaria para responder a suscripciones de
     * los jugadores, así como cancelarlas.
     *
     */
    protected class GanarPartida extends SubscriptionResponder {

        private SubscriptionResponder.Subscription suscripcion;

        public GanarPartida(Agent agente, MessageTemplate plantilla, SubscriptionResponder.SubscriptionManager gestor) {

            super(agente, plantilla, gestor);

        }

        //Método que maneja la suscripcion//
        @Override
        protected ACLMessage handleSubscription(ACLMessage propuesta) throws NotUnderstoodException, RefuseException {
            try {
                ContentElement ce = manager.extractContent(propuesta);
                if (ce instanceof Action) {
                    Action accion = (Action) ce;
                    Partida _p = (Partida) accion.getAction();
                    AID _sender = accion.getActor();
                    if (tableroJugador.containsKey(_p.getIdPartida())) {
                        tableroJugador.get(_p.getIdPartida()).add(_sender);
                    } else {
                        ArrayList< AID> nuevaLista = new ArrayList<>();
                        nuevaLista.add(_sender);
                        tableroJugador.put(_p.getIdPartida(), nuevaLista);
                    }
                    //Hacemos la suscripción//
                    suscripcion = createSubscription(propuesta);
                    mySubscriptionManager.register(suscripcion);
                }
            } catch (Codec.CodecException | OntologyException | RefuseException ex) {
                ex.printStackTrace();
            }
            //Devolvemos Agree//
            ACLMessage agree = propuesta.createReply();
            agree.setPerformative(ACLMessage.AGREE);
            return agree;
        }

        //Maneja la cancelación de la suscripcion//
        @Override
        protected ACLMessage handleCancel(ACLMessage cancelacion) {
            try {
                mySubscriptionManager.deregister(suscripcion);
            } catch (FailureException ex) {
                Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Map.Entry e : tableroJugador.entrySet()) {
                ArrayList<AID> vector = (ArrayList<AID>) e.getValue();
                for (int i = 0; i < vector.size(); i++) {
                    if (cancelacion.getSender().getLocalName().equals(vector.get(i).getLocalName())) {
                        tableroJugador.get(e.getKey()).remove(i);
                        break;
                    }
                }
                if (vector.isEmpty()) {
                    tableroJugador.remove(e.getKey());
                }
            }
            estaJugando.put(cancelacion.getSender(), false);
            ACLMessage cancelar = cancelacion.createReply();
            cancelar.setPerformative(ACLMessage.INFORM);
            return cancelar;
        }
    }

    // ---- CLASES CON FUNCIONALIDAD EXTRA ---- //
    /**
     * Clase para refrescar la información que se encuentra en el tablero.
     *
     */
    protected class refrescarTableros extends TickerBehaviour {

        private Integer rotacion;

        public refrescarTableros(Agent a, long period) {
            super(a, period);
            rotacion = 0;
        }

        @Override
        protected void onTick() {
            //Obtenemos el movimiento a realizar para una de las partidas activas.
            //Se irá rotando la partida a actualizar. 
            if (!colaMovimientos.isEmpty()) {

                PartidaQuoridor pActual = partidas.get(rotacion);
                MovimientoRealizado mRealizado = colaMovimientos.get(pActual.getIdPartida()).get(0);
                if (mRealizado != null) {
                    //Enviamos el movimiento a refrescar//
                    pActual.realizarMovimiento(mRealizado);

                    if (mRealizado.getMovimiento().getElementoJuego() instanceof Muro) {
                        AID jAidd = mRealizado.getJugador().getAgenteJugador();
                        pActual.cambiarNbarreras(pActual.getColor(jAidd), 1, jAidd);
                    }
                    //Eliminamos movimiento de la estructura//
                    colaMovimientos.get(pActual.getIdPartida()).remove(0);
                    if (colaMovimientos.get(pActual.getIdPartida()).isEmpty()) {
                        pActual.ganadorPartida(mRealizado.getJugador().getAgenteJugador());

                        colaMovimientos.remove(pActual.getIdPartida());
                        partidas.remove(pActual);
                    }
                }

                //Aumentamos el índice y controlamos que no supere el máximo índice//
                if (!partidas.isEmpty()) {
                    rotacion++;
                    rotacion = rotacion % partidas.size();
                }
            }
        }
    }

    /**
     * Clase que hereda de "OneShotBehaviour" encargada de notificar el ganador
     * de la partida, así como de iniciar las desuscripciones.
     *
     */
    protected class NotificarGanador extends OneShotBehaviour {

        private Jugador ganador;
        private PartidaQuoridor partida;

        public NotificarGanador(Jugador j, PartidaQuoridor p) {
            ganador = j;
            partida = p;
        }

        @Override
        public void action() {
            //Establecemos el mensaje//
            ACLMessage cancelacion = new ACLMessage(ACLMessage.INFORM);

            cancelacion.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
            cancelacion.setLanguage(codec.getName());
            cancelacion.setOntology(ontologia.getName());

            //Creamos el predicado GanadorPartida//
            Tablero tablero = new Tablero(9, 9);
            Partida mPartida = new Partida(
                    partida.getIdPartida(),
                    juegoQuoridor.OntologiaQuoridor.TIPO_JUEGO,
                    partida.getNjugadores(),
                    tablero);

            GanadorPartida gp = new GanadorPartida(ganador, mPartida);

            try {
                //Llenamos el mensaje e iniciamos la desuscripción de los jugadores//
                manager.fillContent(cancelacion, gp);
                ArrayList<AID> jugadores = partida.getAllJugadores();
                Iterator<Subscription> it = suscripciones.iterator();
                while (it.hasNext()) {
                    Subscription s = it.next();
                    for (int i = 0; i < jugadores.size(); i++) {
                        if (s.getMessage().getSender().getLocalName().equals(jugadores.get(i).getLocalName())) {
                            s.notify(cancelacion);
                        }
                    }
                }
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
