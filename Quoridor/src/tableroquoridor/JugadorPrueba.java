/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tableroquoridor;

import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.BeanOntologyException;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetResponder;
import jade.proto.ProposeResponder;
import jade.proto.SubscriptionInitiator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import juegoQuoridor.elementos.FichaEntregada;
import juegoQuoridor.elementos.JugarPartida;
import juegoQuoridor.elementos.Movimiento;
import juegoQuoridor.elementos.MovimientoRealizado;
import juegosTablero.elementos.Ficha;
import juegosTablero.elementos.Jugador;
import juegosTablero.elementos.Partida;
import juegosTablero.elementos.Posicion;
import juegoQuoridor.elementos.Muro;
import juegosTablero.elementos.GanadorPartida;
import juegosTablero.elementos.ProponerPartida;

/**
 *
 * @author ManuJGQ
 */
public class JugadorPrueba extends Agent {

    //Variables del agente
    private Codec codec = new SLCodec();
    private Ontology ontologia;
    private ContentManager manager = (ContentManager) getContentManager();

    private Ficha ficha;
    private Posicion posicionInicial;

    private ArrayList<Pair<Integer, Integer>> posiblesMovimentos;
    private Posicion posicionArealizar;
    private boolean Ponerbarrera;
    private String alineac;

    /**
     * Variables Inteligencia Jugador
     */
    private ArrayList<ArrayList<Casilla>> tableroJugador;
    private HashMap<AID, Posicion> posicionesEnemigas;
    public boolean estoyJugando;
    private int numerosEnemigos;
    public int nBarrera;
    private Partida partida;

    @Override
    protected void setup() {
        //Inicialización de las variables del agente

        try {
            ontologia = juegoQuoridor.OntologiaQuoridor.getInstance();
        } catch (BeanOntologyException ex) {
            Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Configuración del GUI
        //Registro del agente en las Páginas Amarrillas
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(juegoQuoridor.OntologiaQuoridor.REGISTRO_JUGADOR);
        sd.setName(this.getLocalName());
        sd.addOntologies(juegoQuoridor.OntologiaQuoridor.ONTOLOGY_NAME);
        sd.addLanguages(FIPANames.ContentLanguage.FIPA_SL);
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        //Registro de la Ontología
        manager.registerLanguage(codec);
        manager.registerOntology(ontologia);

        posiblesMovimentos = new ArrayList<>();
        Ponerbarrera = false;
        posicionesEnemigas = new HashMap<>();
        estoyJugando = false;

        //Añadir las tareas principales
        MessageTemplate template = ContractNetResponder.createMessageTemplate(
                FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
        MessageTemplate template2 = ContractNetResponder.createMessageTemplate(
                FIPANames.InteractionProtocol.FIPA_PROPOSE);
        addBehaviour(new ReceptorContractNet(this, template));
        addBehaviour(new ReceptorPropose(this, template2));

    }

    @Override
    protected void takeDown() {
        //Desregristo del agente de las Páginas Amarillas
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            ex.printStackTrace();
        }
        //Liberación de recursos, incluido el GUI

        //Despedida
        System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }

    //Métodos de trabajo del agente
    //Clases internas que representan las tareas del agente
    private class ReceptorContractNet extends ContractNetResponder {

        public ReceptorContractNet(Agent a, MessageTemplate mt) {
            super(a, mt);
        }

        @Override
        protected ACLMessage handleCfp(ACLMessage cfp) {
            try {
                ContentElement c = manager.extractContent(cfp);

                if (!estoyJugando && c instanceof Action) {
                    Action accion = (Action) c;
                    ProponerPartida prop = (ProponerPartida) accion.getAction();
                    ACLMessage acepto = cfp.createReply();
                    acepto.setPerformative(ACLMessage.PROPOSE);
                    estoyJugando = true;

                    //Inicializamos valores de la partida
                    partida = prop.getPartida();
                    numerosEnemigos = partida.getNumeroJugadores() - 1;
                    if (numerosEnemigos == 3) {
                        nBarrera = 5;
                    } else {
                        nBarrera = 10;
                    }
                    return acepto;
                }
                ACLMessage rechazo = cfp.createReply();
                rechazo.setPerformative(ACLMessage.REFUSE);
                return rechazo;

            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(JugadorPrueba.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

        @Override
        protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) throws FailureException {
            try {
                //AQUI RECOGE LA FICHA
                ContentElement f = manager.extractContent(accept);
                if (f instanceof FichaEntregada) {
                    FichaEntregada fe = (FichaEntregada) f;
                    ficha = fe.getFicha();
                    switch (fe.getNumeroJugador()) {
                        case 1:
                            posicionInicial = new Posicion(0, 4);
                            break;
                        case 3:
                            posicionInicial = new Posicion(4, 8);
                            break;
                        case 2:
                            posicionInicial = new Posicion(8, 4);
                            break;
                        case 4:
                            posicionInicial = new Posicion(4, 0);
                            break;
                        default:
                            break;
                    }
                    //Inicializo Tablero
                    tableroJugador = new ArrayList<>(9);
                    for (int i = 0; i < 9; i++) {
                        tableroJugador.add(new ArrayList<>(9));
                        for (int j = 0; j < 9; j++) {
                            tableroJugador.get(i).add(new Casilla());
                        }
                    }

                } else {
                    throw new FailureException("Exception!");
                }

                //Iniciamos suscripcion//
                ContentElement ce = manager.extractContent(cfp);
                if (ce instanceof Action) {
                    Action accion = (Action) ce;
                    ProponerPartida prop = (ProponerPartida) accion.getAction();

                    AID sender = accion.getActor();
                    Partida p = prop.getPartida();

                    addBehaviour(new iniciarSuscripcion(p, sender));
                } else {
                    throw new FailureException("Exception!");
                }

                //Rellenamos estructura de representación//
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(JugadorPrueba.class.getName()).log(Level.SEVERE, null, ex);
            }

            ACLMessage inform = accept.createReply();
            inform.setPerformative(ACLMessage.INFORM);
            return inform;
        }

        @Override
        protected void handleRejectProposal(ACLMessage cfp, ACLMessage propose, ACLMessage reject) {
            //Hemos sido rechazados de la partida
            System.out.println(myAgent.getLocalName());
        }

    }

    private class ReceptorPropose extends ProposeResponder {

        public ReceptorPropose(Agent a, MessageTemplate mt) {
            super(a, mt);
        }

        //Preparación de la respuesta. Recibe un mensaje PROPOSE y, según su contenido, acepta o no.
        @Override
        protected ACLMessage prepareResponse(ACLMessage propuesta) throws NotUnderstoodException {
            try {
                ContentElement c = manager.extractContent(propuesta);
                if (c instanceof Action) {
                    JugarPartida jp;
                    jp = (JugarPartida) ((Action) c).getAction();
                    //AQUI SE EXTRAE EL MOVIMIENTO ANTERIOR Y ACTUALIZA LA ESTRUCTURA DE TABLERO
                    if (jp.getMovimientoAnterior().getElementoJuego() != null) {
                        if (jp.getMovimientoAnterior().getElementoJuego() instanceof Jugador) {
                            if (!((Jugador) jp.getMovimientoAnterior().getElementoJuego()).getAgenteJugador().getName().equals(myAgent.getAID().getName())) {
                                System.out.println(((Jugador) jp.getMovimientoAnterior().getElementoJuego()).getAgenteJugador().getLocalName() + " - " + myAgent.getAID().getLocalName());
                                actualizarMatriz(jp.getMovimientoAnterior());
                            }
                        }
                    }

                    if (jp.getJugadorActivo().getAgenteJugador().getName().equals(myAgent.getAID().getName())) {
                        //Aceptación de la propuesta.
                        System.out.printf("%s: Propuesta aceptada.\n", this.myAgent.getLocalName());

                        //Se crea la respuesta al mensaje con la performativa ACCEPT_PROPOSAL, pues se acepta.
                        ACLMessage agree = propuesta.createReply();
                        agree.setPerformative(ACLMessage.ACCEPT_PROPOSAL);

                        //Construimos el contenido del mensaje
                        Jugador jugador = new Jugador(myAgent.getAID(), ficha);
                        Movimiento movimiento = new Movimiento();
                        heuristicaPonerBarrera();
                        System.out.println(posicionesEnemigas.size() + " - " + numerosEnemigos);
                        if (Ponerbarrera && nBarrera > 0 && posicionesEnemigas.size() == numerosEnemigos) { // Poner barrera y no mueve ficha  
                            movimiento = colocarMuro();
                            nBarrera--;

                        } else { // Movimiento de la ficha
                            Posicion posi;
                            posi = damePosicionAleatoria(posicionInicial.getCoorX(), posicionInicial.getCoorY());
                            Posicion posicion = new Posicion(
                                    posi.getCoorX(),
                                    posi.getCoorY()
                            );
                            posiblesMovimentos.clear();
                            movimiento = new Movimiento(
                                    new Jugador(myAgent.getAID(), ficha), // private Object elementoJuego;   /                                      /AQUI SE CONSTRUYE
                                    //new Posicion(posicionInicial.getCoorX()-1, posicionInicial.getCoorY()) //solo movimiento arriba                    //EL MOVIMIENTO SEGUN
                                    posicion
                            );
                            posicionInicial = movimiento.getPosicion();
                        }

                        MovimientoRealizado mr = new MovimientoRealizado(jugador, movimiento);

                        try {
                            //Enviamos el Movimiento
                            manager.fillContent(agree, mr);
                            return agree;
                        } catch (Codec.CodecException | OntologyException ex) {
                            Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        //Rechazo de la propuesta.
                        //                        System.out.printf("%s: Propuesta rechazada.\n", this.myAgent.getLocalName());

                        //Se crea la respuesta al mensaje con la performativa REJECT_PROPOSAL, pues se rechaza.
                        ACLMessage refuse = propuesta.createReply();
                        refuse.setPerformative(ACLMessage.REJECT_PROPOSAL);

                        return refuse;
                    }
                } else {
                    throw new NotUnderstoodException("Exception!");
                }
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(AgenteTablero.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public void actualizarMatriz(Movimiento m) {
        if (m.getElementoJuego() instanceof Jugador) {
            tableroJugador.get(m.getPosicion().getCoorX()).get(m.getPosicion().getCoorY()).actualizarJugador(((Jugador) m.getElementoJuego()).getAgenteJugador());
            if (((Jugador) m.getElementoJuego()).getAgenteJugador() != null) {
                Posicion posicionAnteriorEnemiga = posicionesEnemigas.getOrDefault(((Jugador) m.getElementoJuego()).getAgenteJugador(), null);
                if (posicionAnteriorEnemiga != null) {
                    tableroJugador.get(posicionAnteriorEnemiga.getCoorX()).get(posicionAnteriorEnemiga.getCoorY()).actualizarJugador(((Jugador) m.getElementoJuego()).getAgenteJugador());
                }
                posicionesEnemigas.put(((Jugador) m.getElementoJuego()).getAgenteJugador(), m.getPosicion());
            }
        }
        if (m.getElementoJuego() instanceof Muro) {
            Muro muro = (Muro) m.getElementoJuego();
            if (muro.getAlineacion().equals(juegoQuoridor.OntologiaQuoridor.ALINEACION_HORIZONTAL)) {
                tableroJugador.get(m.getPosicion().getCoorX()).get(m.getPosicion().getCoorY()).ponerMHorizontal();
            } else {
                tableroJugador.get(m.getPosicion().getCoorX()).get(m.getPosicion().getCoorY()).ponerMVertical();
            }
        }
    }

    public Movimiento colocarMuro() {
        Random rand = new Random();
        int nJugador = rand.nextInt(numerosEnemigos) + 1;
        System.out.println(nJugador + "---------------");
        Posicion p = null;
        Iterator it = posicionesEnemigas.entrySet().iterator();
        for (int i = 0; i < nJugador; i++) {
            Map.Entry pair = (Map.Entry) it.next();
            p = (Posicion) pair.getValue();
        }
        String alineacion;
        int aleatorio = rand.nextInt(2);
        if (aleatorio == 0) {
            alineacion = juegoQuoridor.OntologiaQuoridor.ALINEACION_VERTICAL;
        } else {
            alineacion = juegoQuoridor.OntologiaQuoridor.ALINEACION_HORIZONTAL;
        }
        p = heuristicaPosicionBarrera(p, alineacion);
        Muro muro = new Muro(alineacion);
        return new Movimiento(
                muro,
                p
        );
    }

    public ArrayList<Pair<Integer, Integer>> PosiblesMovimientos(int posInicFila, int posInicColum) {
        Pair<Integer, Integer> Movim; //Movim<K,V>  // key,value 

        //Esquinas
        if (posInicFila == 0 && posInicColum == 0) {
            Movim = new Pair<>(0, 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(1, 0);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }
        if (posInicFila == 0 && posInicColum == 8) {
            Movim = new Pair<>(0, 7);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(1, 8);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }
        if (posInicFila == 8 && posInicColum == 0) {
            Movim = new Pair<>(7, 0);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(8, 1);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }
        if (posInicFila == 8 && posInicColum == 8) {
            Movim = new Pair<>(7, 8);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(8, 7);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        // Borde Superior
        if (posInicFila == 0 && posInicColum > 0 && posInicColum < 8) {
            Movim = new Pair<>(posInicFila, posInicColum - 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila, posInicColum + 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila + 1, posInicColum);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        // Borde Inferior
        if (posInicFila == 8 && posInicColum > 0 && posInicColum < 8) {
            Movim = new Pair<>(posInicFila, posInicColum - 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila, posInicColum + 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila - 1, posInicColum);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        // Borde Izquierda
        if (posInicColum == 0 && posInicFila > 0 && posInicFila < 8) {
            Movim = new Pair<>(posInicFila - 1, posInicColum);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila + 1, posInicColum);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila, posInicColum + 1);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        // Borde Derecha
        if (posInicColum == 8 && posInicFila > 0 && posInicFila < 8) {
            Movim = new Pair<>(posInicFila - 1, posInicColum);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila + 1, posInicColum);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila, posInicColum - 1);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        //En mitad del tablero
        if (posInicFila > 0 && posInicFila < 8 && posInicColum > 0 && posInicColum < 8) {
            Movim = new Pair<>(posInicFila, posInicColum - 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila, posInicColum + 1);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila - 1, posInicColum);
            posiblesMovimentos.add(Movim);
            Movim = new Pair<>(posInicFila + 1, posInicColum);
            posiblesMovimentos.add(Movim);
            return posiblesMovimentos;
        }

        //else - Nunca se produce 
        Movim = new Pair<>(0, 0);
        posiblesMovimentos.add(Movim);
        return posiblesMovimentos;

    }

    //Devuelve posicion aleatoria  
    public Posicion damePosicionAleatoria(int posActualx, int posActualy) {
        ArrayList<Pair<Integer, Integer>> pMovimientos = PosiblesMovimientos(posActualx, posActualy);
        int aleatorio = (int) (Math.random() * (pMovimientos.size()) + 0);
        //System.out.println("---- " + pMovimientos.get(aleatorio).getKey() +"   --- " + pMovimientos.get(aleatorio).getValue());
        Posicion pARealizar = new Posicion(pMovimientos.get(aleatorio).getKey(), pMovimientos.get(aleatorio).getValue());
        return pARealizar;
    }

    public void heuristicaPonerBarrera() {
        int aleatorio = (int) (Math.random() * 2 + 0);
        if (aleatorio == 0) {
            Ponerbarrera = true;
        } else {
            Ponerbarrera = false;
        }
    }

    public String heuristicaAlineacionBarrera() {
        // Calcula alineacion    
        String alineacio;
        int aleatorio2 = (int) (Math.random() * 2 + 0);
        if (aleatorio2 == 0) {
            alineacio = juegoQuoridor.OntologiaQuoridor.ALINEACION_VERTICAL;
        } else {
            alineacio = juegoQuoridor.OntologiaQuoridor.ALINEACION_HORIZONTAL;
        }
        return alineacio;
    }

    public Posicion heuristicaPosicionBarrera(Posicion p, String alineacion) {
        int x = 0;
        int y = 0;
        Random rand = new Random();
        boolean hayBarrera = false;
        do {
            if (p.getCoorX() == 7 && p.getCoorY() == 7) {

                x = rand.nextInt(1) + 6;
                y = rand.nextInt(1) + 6;

            } else if (p.getCoorX() == 7) {

                x = rand.nextInt(1) + 6;
                y = rand.nextInt(8);

            } else {

                y = rand.nextInt(1) + 6;
                x = rand.nextInt(8);

            }
            if (alineacion.equals(juegoQuoridor.OntologiaQuoridor.ALINEACION_VERTICAL)) {
                hayBarrera = tableroJugador.get(x).get(y).hayMVertical();
            } else {
                hayBarrera = tableroJugador.get(x).get(y).hayMHorizontal();
            }

        } while (hayBarrera);

        return new Posicion(x, y);
    }

    /**
     * Clases necesarias para llevar a cabo el protocolo Subscribe.
     *
     */
    protected class iniciarSuscripcion extends OneShotBehaviour {

        private Partida partidaASuscribir;
        private AID sender;

        public iniciarSuscripcion(Partida _p, AID _sender) {
            partidaASuscribir = _p;
            sender = _sender;
        }

        @Override
        public void action() {
            ACLMessage mensaje = new ACLMessage(ACLMessage.SUBSCRIBE);
            mensaje.setProtocol(FIPANames.InteractionProtocol.FIPA_SUBSCRIBE);
            mensaje.addReceiver(sender);
            mensaje.setLanguage(codec.getName());
            mensaje.setOntology(ontologia.getName());
            try {
                Action accion = new Action();
                accion.setAction(partidaASuscribir);
                accion.setActor(myAgent.getAID());

                manager.fillContent(mensaje, accion);
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(JugadorPrueba.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Añadir el comportamiento
            addBehaviour(new Suscripcion(myAgent, mensaje));
        }

    }

    protected class Suscripcion extends SubscriptionInitiator {

        public Suscripcion(Agent agente, ACLMessage mensaje) {
            super(agente, mensaje);
        }

        //Maneja la informacion enviada: INFORM
        @Override
        protected void handleInform(ACLMessage inform) {
            try {
                ContentElement winner = manager.extractContent(inform);
                if (winner instanceof GanadorPartida) {
                    estoyJugando = false;
                    //Enviamos la cancelación//
                    this.cancel(inform.getSender(), true);
                }
            } catch (Codec.CodecException | OntologyException ex) {
                Logger.getLogger(JugadorPrueba.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Maneja la respuesta en caso de fallo: FAILURE
        @Override
        protected void handleFailure(ACLMessage failure) {
            if (failure.getSender().equals(myAgent.getAMS())) {
                System.out.println("El destinatario no existe.");
            } else {
                System.out.println("El agente %s falló al intentar realizar la acción solicitada.");
            }
        }
    }

    public class Casilla {

        private AID jugador;
        private boolean mHorizontal;
        private boolean mVertical;

        public Casilla() {
            jugador = null;
            mHorizontal = false;
            mVertical = false;
        }

        public void actualizarJugador(AID _jugador) {
            if (jugador != null) {
                jugador = null;
            } else {
                jugador = _jugador;
            }
        }

        public void ponerMHorizontal() {
            mHorizontal = true;
        }

        public void ponerMVertical() {
            mVertical = true;
        }

        public boolean hayJugador() {
            return jugador != null;
        }

        public boolean hayMHorizontal() {
            return mHorizontal;
        }

        public boolean hayMVertical() {
            return mVertical;
        }
    }
}

/*
 To do: 
 - Como controlar cuando acaba una partida: ganador, jugadores procesan mas movimientos, etc...
 - Estructura de información
 - comprobar si la posicion adyacente hay un jugador o barrera.
 - si hay jugador sobrepasarlo.
 - si hay barrera que no se pueda sobrepasar.
 */
