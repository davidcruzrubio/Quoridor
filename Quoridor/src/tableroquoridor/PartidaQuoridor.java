/**
 * Fichero en el que encapsulamos la clase necesaria para que nuestro
 * agente tablero pueda alojar varias Partidas al mismo tiempo, se encapsulan
 * en objetos de esta clase.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
package tableroquoridor;

import interfaztablero.TableroGUI;
import jade.core.AID;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import juegoQuoridor.elementos.MovimientoRealizado;
import juegoQuoridor.elementos.Muro;
import juegosTablero.elementos.Ficha;
import juegosTablero.elementos.Jugador;
import juegosTablero.elementos.Posicion;

/**
 * Clase que representa una Partida en el agente tablero.
 *
 * @author Manuel Jesús García Quesada, José Ortega Donaire y David Cruz Rubio
 */
public class PartidaQuoridor {

    private TableroGUI tablero;
    private Map<AID, String> jugadores;
    private String idPartida;
    private ArrayList<AID> descalificados;

    int nBarrerasRojo; //Cantidad de paredes restantes del jugador Rojo
    int nBarrerasAmarillo; //Cantidad de paredes restantes del jugador Amarillo
    int nBarrerasAzul; //Cantidad de paredes restantes del jugador Azul
    int nBarrerasVerde; //Cantidad de paredes restantes del jugador Verde

    /**
     * Método constructor de la clase
     *
     * @param at
     * @param _idPartida
     */
    public PartidaQuoridor(AgenteTablero at, String _idPartida) {
        tablero = new TableroGUI(at);
        jugadores = new HashMap<>();
        descalificados = new ArrayList<>();
        idPartida = _idPartida;
    }

    /**
     * Introduce un jugador en el mapa de jugadores
     *
     * @param aid
     */
    public void insertarJugador(AID aid) {
        jugadores.put(aid, null);
    }

    /**
     * Asignamos los colores a los jugadores.
     *
     * @param aid
     * @param color
     */
    public void asignarColor(AID aid, String color) {
        switch (color) {
            case "Rojo":
                jugadores.put(aid, color);
                tablero.insertarJugador(color, aid.getLocalName());
                break;
            case "Verde":
                jugadores.put(aid, color);
                tablero.insertarJugador(color, aid.getLocalName());
                break;
            case "Azul":
                jugadores.put(aid, color);
                tablero.insertarJugador(color, aid.getLocalName());
                break;
            default:
                jugadores.put(aid, "Amarillo");
                tablero.insertarJugador("Amarillo", aid.getLocalName());
        }
    }

    /**
     * Función que busca un jugador por AID y lo devuelve si lo encuentra
     *
     * @param aid
     * @return
     */
    public AID buscarJugador(AID aid) {
        if (jugadores.containsKey(aid)) {
            Iterator it = jugadores.entrySet().iterator();
            while (it.hasNext()) {
                if (it.next() == aid) {
                    return aid;
                }
            }
        }
        return null;
    }

    /**
     * Función que devuelve un ArrayList con los AID de los jugadores, se
     * comprueban si los jugadores están descalificados.
     *
     * @return
     */
    public ArrayList<AID> getJugadores() {
        ArrayList<AID> js = new ArrayList<AID>();
        Iterator<AID> it = jugadores.keySet().iterator();
        while (it.hasNext()) {
            AID aid = (AID) it.next();
            boolean descalificado = false;
            for (int i = 0; i < descalificados.size(); i++) {
                if (aid == descalificados.get(i)) {
                    descalificado = true;
                    break;
                }
            }
            if (!descalificado) {
                js.add(aid);  // Acceder al aid del MAP   
            }
        }
        return js;

    }

    /**
     * Función similar a la anterior, sin embargo no se comprueba si un jugador
     * está eliminado.
     *
     * @return
     */
    public ArrayList<AID> getAllJugadores() {
        ArrayList<AID> js = new ArrayList<AID>();
        Iterator<AID> it = jugadores.keySet().iterator();
        while (it.hasNext()) {
            AID aid = (AID) it.next();
            js.add(aid);  // Acceder al aid del MAP  
        }
        return js;
    }

    /**
     * Función para obtener el número de jugadores.
     *
     * @return
     */
    public int getNjugadores() {
        return jugadores.size();
    }

    /**
     * Función para obtener el Id de una partida.
     *
     * @return
     */
    public String getIdPartida() {
        return idPartida;
    }

    /**
     * Función que determinando un turno devuelve el AID del jugador.
     *
     * @param turno
     * @return
     */
    public AID getJugador(int turno) {
        String color = "";
        if (turno == 1 && jugadores.size() == 2) {
            turno++;
        }
        switch (turno) {
            case 0:
                color = "Rojo";
                break;
            case 1:
                color = "Azul";
                break;
            case 2:
                color = "Verde";
                break;
            case 3:
                color = "Amarillo";
            default:
                break;
        }
        Iterator it = jugadores.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getValue().equals(color)) {
                return ((AID) pair.getKey());
            }
        }
        return null;
    }

    /**
     * Función que determinando el AID devuelve el color del jugador.
     *
     * @param aid
     * @return
     */
    public String getColor(AID aid) {
        String color = jugadores.get(aid);
        if ("Amarillo".equals(color)) {
            return "Negro";
        } else {
            return color;
        }

    }

    /**
     * Función que descalifica a un jugador mediante su AID.
     *
     * @param aid
     */
    public void descalificar(AID aid) {
        descalificados.add(aid);
        tablero.descalificar(aid.getLocalName(), jugadores.get(aid));
    }

    /**
     * Comprueba si un jugador está descalificado o no.
     *
     * @param aid
     * @return
     */
    public boolean comprobarDescalificado(AID aid) {
        for (int i = 0; i < descalificados.size(); i++) {
            if (descalificados.get(i) == aid) {
                return true;
            }
        }
        return false;
    }

    /**
     * Función que establece una cantidad de barreras dependiendo del número de
     * jugadores.
     *
     * @param aid
     * @param color
     * @param njug
     */
    public void comprobarCantidadBarreras(AID aid, String color, int njug) {
        if (njug == 2) {
            tablero.setnBarreras(color, 10);
            setnBarreras(color, 10);
        }
        if (njug == 4) {
            tablero.setnBarreras(color, 5);
            setnBarreras(color, 5);
        }
    }

    /**
     * Función que establece a un jugador en concreto el número de barreras
     * determinado.
     *
     * @param color
     * @param n
     */
    public void setnBarreras(String color, int n) {
        switch (color) {
            case "Rojo":
                nBarrerasRojo = n;
                break;
            case "Negro":
                nBarrerasAmarillo = n;
                break;
            case "Verde":
                nBarrerasVerde = n;
                break;
            case "Azul":
                nBarrerasAzul = n;
                break;
        }
    }

    /**
     * Función para poder modificar el número de barreras de un jugador
     * determinado por su AID.
     *
     * @param color
     * @param nBarrerasAdecrementar
     * @param aidJugador
     */
    public void cambiarNbarreras(String color, int nBarrerasAdecrementar, AID aidJugador) {
        switch (color) {
            case "Rojo":
                tablero.setnBarreras(color, nBarrerasRojo - nBarrerasAdecrementar);
                setnBarreras(color, nBarrerasRojo - nBarrerasAdecrementar);
                break;
            case "Negro":
                tablero.setnBarreras(color, nBarrerasAmarillo - nBarrerasAdecrementar);
                setnBarreras(color, nBarrerasAmarillo - nBarrerasAdecrementar);
                break;
            case "Verde":
                tablero.setnBarreras(color, nBarrerasVerde - nBarrerasAdecrementar);
                setnBarreras(color, nBarrerasVerde - nBarrerasAdecrementar);
                break;
            case "Azul":
                tablero.setnBarreras(color, nBarrerasAzul - nBarrerasAdecrementar);
                setnBarreras(color, nBarrerasAzul - nBarrerasAdecrementar);
                break;
        }
        tablero.setTextoConsola(color, aidJugador.getLocalName());
    }

    /**
     * Función que se encarga de realizar los movimientos en el tablero gráfico.
     *
     * @param mr
     */
    public void realizarMovimiento(MovimientoRealizado mr) {
        Posicion posicion = mr.getMovimiento().getPosicion();
        Object objetoJuego = mr.getMovimiento().getElementoJuego();

        //Comprobamos si el objeto es un Muro, con lo cual activaremos
        //un muro en la posición y con la alineación determinadas.
        if (objetoJuego instanceof Muro) {
            String alineacion = ((Muro) objetoJuego).getAlineacion();
            tablero.establecerBarrera(posicion, alineacion, mr.getJugador().getAgenteJugador());

            //Comprobamos ahora si el objeto es de tipo Jugador, con lo cual
            //tendremos que cambiar al jugador de sitio.
        } else if (objetoJuego instanceof Jugador) {
            Ficha f = ((Jugador) objetoJuego).getFicha();
            String color = f.getColor();
            tablero.moverJugador(posicion, color, mr.getJugador().getAgenteJugador());
        }
    }

    /**
     * Función que muestra el ganador de la partida mediante su AID.
     *
     * @param aid
     */
    public void ganadorPartida(AID aid) {
        tablero.mostrarGanador(aid);
    }

}
